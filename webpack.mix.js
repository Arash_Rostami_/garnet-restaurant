const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/vue.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .scripts([
        // 'resources/js/second.js',
        'resources/js/third.js',
        'resources/js/fourth.js',
        'resources/js/fifth.js',
        'resources/js/sixth.js',
        'resources/js/seventh.js',
        'resources/js/eighth.js',
        'resources/js/last.js',
        'resources/js/final.js',
        'resources/js/video-js.js',
    ], 'public/js/final.js');

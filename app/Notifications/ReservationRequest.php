<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class ReservationRequest extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->name = $request->userName;
        $this->phone = $request->phoneNumber;
        $this->person = $request->partySize;
        $this->date = $request->startDate;
        $this->time = $request->resTime;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('رزرو میز')
            ->greeting('Hi')
            ->line(new HtmlString('<strong>User: </strong>'))
            ->line( $this->name)
            ->line(new HtmlString('<strong>Phone: </strong>'))
            ->line( $this->phone)
            ->line(new HtmlString('<strong>Guests: </strong>'))
            ->line( $this->person)
            ->line(new HtmlString('<strong>Time: </strong>'))
            ->line( $this->time)
            ->line(new HtmlString('<strong>Date: </strong>'))
            ->line( $this->date);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

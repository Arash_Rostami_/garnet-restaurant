<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookingRequest;
use App\Http\Requests\ContactRequest;
use App\Models\Question;
use App\Models\Reservation;
use App\Notifications\ReservationRequest;
use App\Notifications\UserQuestion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Session;

class ApplicationController extends Controller
{
    public function questions(ContactRequest $request)
    {
        $this->saveQuestionDb($request);
        $this->notifyAdmin($request);
        return back();

    }

    public function reservations(BookingRequest $request)
    {
        $this->saveReservationDb($request);
        $this->notifyAdminReservation($request);
        return back();


    }


    public function admin(Request $request)
    {
        $reservation = Reservation::orderBy('id', 'DESC')->paginate(10, ['*'], 'rsrv');
        $question = Question::orderBy('id', 'DESC')->paginate(10, ['*'], 'qst');

        return view('admin', compact(['reservation', 'question']));

    }

    public function delete($id)
    {
        $reservation = Reservation::find($id);
        ($reservation->delete()) ? Session::flash('reservation', "success") : Session::flash('reservation', "error");

    }

    public function resolve($id)
    {

        $question = Question::find($id);
        ($question->delete()) ? Session::flash('question', "success") : Session::flash('question', "error");

    }

    public function localization($locale)
    {
        Session::put('locale', $locale);
        return redirect()->back();
    }

    /**
     * @param Request $request
     */
    public function saveQuestionDb(Request $request): void
    {
        $questions = new Question;

        $questions->name = $request->name;
        $questions->email = $request->email;
        $questions->message = $request->message;

        ($questions->save()) ? Session::flash('message', "success") : Session::flash('message', "error");
    }

    /**
     * @param Request $request
     */
    public function notifyAdmin(Request $request): void
    {
        Notification::route('mail', 'info@garnetrestaurant.com')
            ->notify(new UserQuestion($request));
    }

    /**
     * @param BookingRequest $request
     */
    public function saveReservationDb(BookingRequest $request): void
    {
        $reservation = new Reservation;

        $reservation->name = $request->userName;
        $reservation->phone = $request->phoneNumber;
        $reservation->person = $request->partySize;
        $reservation->date = $request->startDate;
        $reservation->time = $request->resTime;


        ($reservation->save()) ? Session::flash('booking', "success") : Session::flash('booking', "error");
    }

    /**
     * @param BookingRequest $request
     */
    public function notifyAdminReservation(BookingRequest $request): void
    {
        Notification::route('mail', 'info@garnetrestaurant.com')
            ->notify(new ReservationRequest($request));
    }

    public function menu()
    {
        return view('menu');

    }
}

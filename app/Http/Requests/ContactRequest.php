<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:4',
            'email' => 'required|email:dns',
            'message' => 'required|max:500',
            'phone' => 'empty'
        ];
    }

    public function messages()
    {
        if (app()->getLocale() == 'pr') {
            return [
                'name.required' => 'درج نام الزامیست!',
                'name.min' => 'نام می بایست کامل باشد!',
                'email.required' => 'ایمیل الزامیست!',
                'email.email' => 'ایمیل صحیح نمی باشد!',
                'message.required' => 'پیامی نوشته نشده است!',
                'message.max' => 'پیام شما از تعداد مجاز بیشتر می باشد!',
                'phone.empty' => 'ای بابا! ول کن.',
            ];
        } else {
            return [
                'name.required' => 'Your full name is required.',
                'name.min' => 'Your full name is NOT complete.',
                'email.required' => 'Your email is required.',
                'email.email' => 'Your email is not in the right format.',
                'message.required' => 'No message is written.',
                'message.max' => 'Message cannot be longer than 500 letters.',
                'phone.empty' => 'Sorry! no place for ROBOTS!',
            ];
        }
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'userName' => 'required|min:4',
            'phoneNumber' => 'required',
            'emailAddress' => 'empty',
            'partySize' => 'integer|max:20',
            'startDate' => 'required',
            'resTime' => 'required'

        ];
    }


    public function messages()
    {
        if (app()->getLocale() == 'pr') {
            return [
                'userName.required' => 'درج نام الزامیست!',
                'userName.min' => 'نام می بایست کامل باشد!',
                'phoneNumber.required' => 'شماره تماس الزامیست!',
                'partySize.integer' => 'تعداد میهمان ها الزامیست!',
                'partySize.max' => 'تعداد میهمان ها بیشتر از فضای موجود است!',
                'startDate.required' => 'تاریخ رزرو الزامیست!',
                'resTime.required' => 'ساعت رزرو الزامیست!',
                'emailAddress.empty' => 'ای بابا! ول کن.',
            ];
        } else {
            return [
                'userName.required' => 'Your full name is required.',
                'userName.min' => 'Your full name is NOT complete.',
                'phoneNumber.required' => 'Your phone number is required.',
                'partySize.integer' => 'You need to give the number of guests.',
                'partySize.max' => 'The number of guests is more than space available.',
                'startDate.required' => 'The date for booking is necessary.',
                'resTime.required' => 'The time for booking is necessary.',
                'emailAddress.empty' => 'Sorry! no place for ROBOTS!',
            ];
        }

    }
}

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<x-header-hidden title="Homepage"/>
<body class="home page-template page-template-full-width page-template-full-width-php page page-id-7 theme-laurent laurent-core-1.0 woocommerce-no-js laurent-ver-1.0.1 eltdf-grid-1300 eltdf-content-is-behind-header eltdf-wide-dropdown-menu-content-in-grid eltdf-no-behavior eltdf-dropdown-animate-height eltdf-header-standard eltdf-menu-area-shadow-disable eltdf-menu-area-in-grid-shadow-disable eltdf-menu-area-in-grid-border-disable eltdf-logo-area-border-disable eltdf-logo-area-in-grid-border-disable eltdf-header-vertical-shadow-disable eltdf-header-vertical-border-disable eltdf-side-menu-slide-from-right eltdf-woocommerce-columns-2 eltdf-woo-normal-space eltdf-woo-pl-info-below-image eltdf-woo-single-thumb-below-image eltdf-woo-single-has-pretty-photo eltdf-default-mobile-header eltdf-sticky-up-mobile-header wpb-js-composer js-comp-ver-6.1 vc_responsive">
<div class="eltdf-wrapper">
    <div class="eltdf-wrapper-inner">
        <div id="localization" title=" {{ __('homepage.lingo') }}">
            <a class="localization-link" href="{{ url('locale/en') }}" ><img class="localization-img"  alt="english_flag" src="/images/en.png"></a>
            <a class="localization-link" href="{{ url('locale/pr') }}" ><img class="localization-img" alt="iran_flag" src="/images/pr.png"></a>
        </div>
        <x-sidelines/>
        <x-nav/>
        <x-nav-mobile/>
        <x-back-to-top/>
        <div class="eltdf-content" style="margin-top: -110px">
            <div class="eltdf-content-inner">
                <div class="eltdf-full-width">
                    <div class="eltdf-full-width-inner">
                        <x-background-marks/>
                        <div class="eltdf-grid-row">
                            <div class="eltdf-page-content-holder eltdf-grid-col-12">
                                <x-slider-main/>
                                <x-about-us/>
                                <x-slider-quote/>
                                <x-mid-image/>
                                <x-selection/>
                                <x-philosophy/>
                                <x-menu-components/>
                                <x-reservation/>
                                <x-modal-condition/>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <x-footer/>

    </div>
</div>
<x-video />
<x-side-menu/>
<x-menu/>
<br>


@include('partials/js')


</body>


</html>


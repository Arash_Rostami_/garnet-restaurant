<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'Laravel')
<img src="{{ asset('images/footer-logo.png') }}" class="logo" alt="Garnet Logo">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>

<header class="eltdf-mobile-header" id="nav-mobile">
    <div class="eltdf-mobile-header-inner">
        <div class="eltdf-mobile-header-holder">
            <div class="eltdf-grid">
                <div class="eltdf-vertical-align-containers">
                    <div class="eltdf-position-left">
                        <div class="eltdf-position-left-inner">
                            <img   width="100px"
                                   src="/images/logo-pure.png"
                                   class="image"
                                   alt="logo"
                            />
                        </div>
                    </div>
                    <div class="eltdf-position-right">
                        <div class="eltdf-position-right-inner">
                            <div class="eltdf-mobile-menu-opener eltdf-mobile-menu-opener-predefined">
                                <a href="javascript:void(0)">
<span class="eltdf-mobile-menu-icon" >
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 37 25.2" class="eltdf-menu-opener"><line y1="7.6" x2="24"
                                                                                              y2="7.6"/>
    <line x1="4.1" y1="0.5" x2="28.1" y2="0.5"/>
    <line x1="10.1" y1="24.6" x2="34.1" y2="24.6"/><line x1="13" y1="17.6" x2="37" y2="17.6"/>
</svg>
</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="eltdf-mobile-nav" role="navigation" aria-label="Mobile Menu">
            <div class="eltdf-grid">
                <ul id="menu-main-menu-1" class="">
                    <li id="mobile-menu-item-13"
                        class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children eltdf-active-item has_sub">
                        <a href="#" class=" current  eltdf-mobile-no-link"><span>{{ __('homepage.menu-1') }}</span></a><span
                                class="mobile_arrow"><svg xmlns="http://www.w3.org/2000/svg"
                                                          viewBox="0 0 8.3 8.5"
                                                          class="eltdf-menu-arrow"><polyline
                                        points="0.4 0.4 3.6 4.2 0.4 8.1 "/><polyline
                                        points="4.5 0.4 7.7 4.2 4.5 8.1 "/></svg></span>
                    </li>
                    <li id="mobile-menu-item-15"
                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub">
                        <a href="#story" class=" eltdf-mobile-no-link"><span>{{ __('homepage.menu-2') }}</span></a><span
                                class="mobile_arrow"><svg xmlns="http://www.w3.org/2000/svg"
                                                          viewBox="0 0 8.3 8.5"
                                                          class="eltdf-menu-arrow"><polyline
                                        points="0.4 0.4 3.6 4.2 0.4 8.1 "/><polyline
                                        points="4.5 0.4 7.7 4.2 4.5 8.1 "/></svg></span>
                    </li>
                    <li id="mobile-menu-item-16"
                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub">
                        <a href="#gallery" class=" eltdf-mobile-no-link"><span>{{ __('homepage.menu-3') }}</span></a><span
                                class="mobile_arrow"><svg xmlns="http://www.w3.org/2000/svg"
                                                          viewBox="0 0 8.3 8.5"
                                                          class="eltdf-menu-arrow"><polyline
                                        points="0.4 0.4 3.6 4.2 0.4 8.1 "/><polyline
                                        points="4.5 0.4 7.7 4.2 4.5 8.1 "/></svg></span>
                    </li>
                    <li id="mobile-menu-item-17"
                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub">
                        <a href="https://garnet.menew.pw/" class=" eltdf-mobile-no-link"><span>{{ __('homepage.menu-4') }}</span></a><span
                                class="mobile_arrow"><svg xmlns="http://www.w3.org/2000/svg"
                                                          viewBox="0 0 8.3 8.5"
                                                          class="eltdf-menu-arrow"><polyline
                                        points="0.4 0.4 3.6 4.2 0.4 8.1 "/><polyline
                                        points="4.5 0.4 7.7 4.2 4.5 8.1 "/></svg></span>
                    </li>
                    <li id="mobile-menu-item-18"
                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub">
                        <a href="#reservation" class=" eltdf-mobile-no-link"><span>{{ __('homepage.menu-5') }}</span></a><span
                                class="mobile_arrow"><svg xmlns="http://www.w3.org/2000/svg"
                                                          viewBox="0 0 8.3 8.5"
                                                          class="eltdf-menu-arrow"><polyline
                                        points="0.4 0.4 3.6 4.2 0.4 8.1 "/><polyline
                                        points="4.5 0.4 7.7 4.2 4.5 8.1 "/></svg></span>
                    </li>
                    <li id="mobile-menu-item-19"
                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub">
                        <a href="#contact" class=" eltdf-mobile-no-link"><span>{{ __('homepage.menu-6') }}</span></a><span
                                class="mobile_arrow"><svg xmlns="http://www.w3.org/2000/svg"
                                                          viewBox="0 0 8.3 8.5"
                                                          class="eltdf-menu-arrow"><polyline
                                        points="0.4 0.4 3.6 4.2 0.4 8.1 "/><polyline
                                        points="4.5 0.4 7.7 4.2 4.5 8.1 "/></svg></span>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>

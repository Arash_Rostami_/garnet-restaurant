<div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12" id="google">
    <div class="vc_column-inner">
        <div class="wpb_wrapper">
            <div class="eltdf-st-title-holder">
                <div class="eltdf-google-map-holder decor">
                    <div class="eltdf-google-map eltdf-map animate__zoomIn animate__animated animate__delay-2s"
                         id="eltdf-map-2641205">
                        <object data="https://maps.google.com/maps?width=520&amp;height=400&amp;hl=en&amp;q=Tehran%20Province,%20Tehran,%20Mehr%20Mohammadi%20Rd,%20Iran%20Tehran+(Garnet%20Luxury%20Cafe%20)&amp;t=&amp;z=15&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"
                                width="520" height="650" type="text/html"></object>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

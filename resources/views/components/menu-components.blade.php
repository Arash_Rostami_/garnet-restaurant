<div id="menu-components">
    <div class="vc_row wpb_row vc_row-fluid vc_custom_1573549706260 eltdf-content-aligment-center" >
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="eltdf-section-title-holder    eltdf-st-decor-animation"
                         style="text-align: center">
                        <img  width="120"
                              src="/images/logo-g.png"
                              class="image wp-image-75  attachment-full size-full"
                              alt="u"
                              loading="lazy"
                        />
                        <div class="eltdf-st-inner">
                            <span class="eltdf-st-tagline">
                             {{ __('homepage.menu-link-head') }}
                            </span>
                            <div class="eltdf-st-title-holder">
                                <div class="decor">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="41.125"
                                         height="9.146">
                                        <path fill="none" stroke="#9C7C57"
                                              stroke-miterlimit="10"
                                              d="M40.881 8.576L20.562.591.244 8.576"/>
                                        <path fill="none" stroke="#9C7C57"
                                              stroke-miterlimit="10"
                                              d="M40.881.591L20.562 8.576.243.591"/>
                                    </svg>
                                </div>
                                <h1 class="eltdf-st-title" id="complete-menu">
                                    {{ __('homepage.menu-link-title') }}
                                </h1>
                                <div class="decor">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="41.125"
                                         height="9.146">
                                        <path fill="none" stroke="#9C7C57"
                                              stroke-miterlimit="10"
                                              d="M40.881 8.576L20.562.591.244 8.576"/>
                                        <path fill="none" stroke="#9C7C57"
                                              stroke-miterlimit="10"
                                              d="M40.881.591L20.562 8.576.243.591"/>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="eltdf-row-grid-section-wrapper eltdf-content-aligment-center" >
        <div class="eltdf-row-grid-section">
            <div class="vc_row wpb_row vc_row-fluid vc_custom_1573574980934">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="eltdf-portfolio-list-holder eltdf-grid-list eltdf-pl-gallery eltdf-three-columns eltdf-medium-space eltdf-disable-bottom-space eltdf-pl-standard-overlay    eltdf-pl-pag-no-pagination  eltdf-pl-has-animation     "
                                 data-type=gallery data-number-of-columns=three
                                 data-space-between-items=medium data-number-of-items=3
                                 data-image-proportions=full
                                 data-enable-fixed-proportions=no
                                 data-enable-image-shadow=no data-category=specialties
                                 data-orderby=date data-order=ASC
                                 data-item-style=standard-overlay data-enable-title=yes
                                 data-title-tag=h5 data-enable-category=yes
                                 data-enable-count-images=yes data-enable-excerpt=no
                                 data-excerpt-length=20 data-pagination-type=no-pagination
                                 data-filter=no data-filter-order-by=name
                                 data-enable-article-animation=yes
                                 data-portfolio-slider-on=no data-enable-loop=yes
                                 data-enable-autoplay=yes data-slider-speed=5000
                                 data-slider-speed-animation=600 data-enable-navigation=yes
                                 data-enable-pagination=yes data-max-num-pages=1
                                 data-next-page=2>
                                <div class="eltdf-pl-inner eltdf-outer-space  clearfix">
                                    <article
                                            class="eltdf-pl-item eltdf-item-space  post-942 portfolio-item type-portfolio-item status-publish has-post-thumbnail hentry portfolio-category-specialties portfolio-tag-recipes portfolio-tag-sweet portfolio-tag-tasty">
                                        <div class="eltdf-pl-item-inner">
                                            <div class="eltdf-pli-image">
                                                <img width="800" height="1217"  id="menu"
                                                     src="/images/main-images-11.jpg"
                                                     class="attachment-full size-full wp-post-image"
                                                     alt="d" loading="lazy"
                                                     srcset="/images/main-images-11.jpg 800w, images/main-images-11-600x913.jpg 600w, images/main-images-11-197x300.jpg 197w, images/main-images-11-768x1168.jpg 768w, images/main-images-11-673x1024.jpg 673w"
                                                     sizes="(max-width: 800px) 100vw, 800px"/>
                                                <div class="eltdf-pli-image-shader-overlay pointer" onclick="menu(1)" title=" {{ __('homepage.menu-link-click') }}">
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                         x="0px" y="0px" width="47.994px"
                                                         height="47.994px"
                                                         viewBox="0 0 47.994 47.994"
                                                         enable-background="new 0 0 47.994 47.994"
                                                         xml:space="preserve">
<line fill="none" stroke="#715B3E" stroke-miterlimit="10" x1="21.044" y1="3" x2="21.044" y2="47.994"/>
                                                        <line fill="none" stroke="#715B3E"
                                                              stroke-miterlimit="10"
                                                              x1="27.044" y1="0" x2="27.044"
                                                              y2="44.994"/>
                                                        <line fill="none" stroke="#715B3E"
                                                              stroke-miterlimit="10"
                                                              x1="44.994" y1="21.484" x2="0"
                                                              y2="21.484"/>
                                                        <line fill="none" stroke="#715B3E"
                                                              stroke-miterlimit="10"
                                                              x1="47.994" y1="26.5" x2="3"
                                                              y2="26.5"/>
</svg>
                                                </div>
                                            </div>
                                            <div class="eltdf-pli-text-holder pointer" onclick="menu(1)">
                                                <div class="eltdf-pli-text-wrapper">
                                                    <div class="eltdf-pli-text">
                                                        <h5 itemprop="name"
                                                            class="eltdf-pli-title entry-title pointer menu">
                                                            {{ __('homepage.menu-link-1') }}

                                                        </h5>
                                                        <div class="eltdf-pli-category-holder">
                                                            <a itemprop="url"
                                                               class="eltdf-pli-category menu">
                                                                {{ __('homepage.menu-link-click') }}
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </article>
                                    <article
                                            class="eltdf-pl-item eltdf-item-space  post-943 portfolio-item type-portfolio-item status-publish has-post-thumbnail hentry portfolio-category-specialties portfolio-tag-sweet portfolio-tag-tasty">
                                        <div class="eltdf-pl-item-inner">
                                            <div class="eltdf-pli-image">
                                                <img width="800" height="1217"
                                                     src="/images/main-images-2.jpg"
                                                     class="attachment-full size-full wp-post-image"
                                                     alt="j" loading="lazy"
                                                     srcset="/images/main-images-2.jpg 800w, images/main-images-2-600x913.jpg 600w, images/main-images-2-197x300.jpg 197w, images/main-images-2-768x1168.jpg 768w, images/main-images-2-673x1024.jpg 673w"
                                                     sizes="(max-width: 800px) 100vw, 800px"/>
                                                <div class="eltdf-pli-image-shader-overlay pointer" onclick="menu(2)">
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                         x="0px" y="0px" width="47.994px"
                                                         height="47.994px"
                                                         viewBox="0 0 47.994 47.994"
                                                         enable-background="new 0 0 47.994 47.994"
                                                         xml:space="preserve">
<line fill="none" stroke="#715B3E" stroke-miterlimit="10" x1="21.044" y1="3" x2="21.044" y2="47.994"/>
                                                        <line fill="none" stroke="#715B3E"
                                                              stroke-miterlimit="10"
                                                              x1="27.044" y1="0" x2="27.044"
                                                              y2="44.994"/>
                                                        <line fill="none" stroke="#715B3E"
                                                              stroke-miterlimit="10"
                                                              x1="44.994" y1="21.484" x2="0"
                                                              y2="21.484"/>
                                                        <line fill="none" stroke="#715B3E"
                                                              stroke-miterlimit="10"
                                                              x1="47.994" y1="26.5" x2="3"
                                                              y2="26.5"/>
</svg>
                                                </div>
                                            </div>
                                            <div class="eltdf-pli-text-holder" onclick="menu(2)">
                                                <div class="eltdf-pli-text-wrapper">
                                                    <div class="eltdf-pli-text">
                                                        <h5 itemprop="name"
                                                            class="eltdf-pli-title entry-title pointer menu">
                                                            {{ __('homepage.menu-link-2') }}

                                                        </h5>
                                                        <div class="eltdf-pli-category-holder pointer menu">
                                                            <a itemprop="url"
                                                               class="eltdf-pli-category">
                                                                {{ __('homepage.menu-link-click') }}
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                    <article
                                            class="eltdf-pl-item eltdf-item-space  post-944 portfolio-item type-portfolio-item status-publish has-post-thumbnail hentry portfolio-category-specialties portfolio-tag-recipes portfolio-tag-sweet portfolio-tag-tasty">
                                        <div class="eltdf-pl-item-inner">
                                            <div class="eltdf-pli-image">
                                                <img width="800" height="1217"
                                                     src="/images/main-images-3.jpg"
                                                     class="attachment-full size-full wp-post-image"
                                                     alt="s" loading="lazy"
                                                     srcset="/images/main-images-3.jpg 800w, images/main-images-3-600x913.jpg 600w, images/main-images-3-197x300.jpg 197w, images/main-images-3-768x1168.jpg 768w, images/main-images-3-673x1024.jpg 673w"
                                                     sizes="(max-width: 800px) 100vw, 800px"/>
                                                <div class="eltdf-pli-image-shader-overlay pointer" onclick="menu(3)">
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                         x="0px" y="0px" width="47.994px"
                                                         height="47.994px"
                                                         viewBox="0 0 47.994 47.994"
                                                         enable-background="new 0 0 47.994 47.994"
                                                         xml:space="preserve">
<line fill="none" stroke="#715B3E" stroke-miterlimit="10" x1="21.044" y1="3" x2="21.044" y2="47.994"/>
                                                        <line fill="none" stroke="#715B3E"
                                                              stroke-miterlimit="10"
                                                              x1="27.044" y1="0" x2="27.044"
                                                              y2="44.994"/>
                                                        <line fill="none" stroke="#715B3E"
                                                              stroke-miterlimit="10"
                                                              x1="44.994" y1="21.484" x2="0"
                                                              y2="21.484"/>
                                                        <line fill="none" stroke="#715B3E"
                                                              stroke-miterlimit="10"
                                                              x1="47.994" y1="26.5" x2="3"
                                                              y2="26.5"/>
</svg>
                                                </div>
                                            </div>
                                            <div class="eltdf-pli-text-holder" onclick="menu(3)">
                                                <div class="eltdf-pli-text-wrapper">
                                                    <div class="eltdf-pli-text">
                                                        <h5 itemprop="name"
                                                            class="eltdf-pli-title entry-title pointer menu">
                                                            {{ __('homepage.menu-link-3') }}
                                                        </h5>
                                                        <div class="eltdf-pli-category-holder pointer menu">
                                                            <a itemprop="url"
                                                               class="eltdf-pli-category">
                                                                {{ __('homepage.menu-link-click') }}
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div data-parallax-bg-image="/images/environment.jpg"
         data-parallax-bg-speed="0.5"
         class="vc_row wpb_row vc_row-fluid vc_custom_1574159245515 eltdf-parallax-row-holder eltdf-content-aligment-center">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="eltdf-video-button-holder  ">
                        <div class="eltdf-video-button-image">   </div>
                        <a class="eltdf-video-button-play pointer" onclick="playMeTwo()">
<span class="eltdf-video-button-play-inner">
<span title="{{ __('homepage.video') }}">
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="97.094px"
     height="97.094px" viewBox="0 0 97.094 97.094" enable-background="new 0 0 97.094 97.094" xml:space="preserve">
<circle fill="none" stroke="currentColor" stroke-miterlimit="10" cx="48.558" cy="48.548" r="48"/>
<circle fill="none" class="eltdf-popout" stroke="none" stroke-miterlimit="10" cx="48.558" cy="48.548" r="41.037"/>
<polygon fill="none" stroke="currentColor" stroke-miterlimit="10" points="42.578,69.964 42.578,27.13 63.994,48.546 "/>
</svg>
</span>
</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

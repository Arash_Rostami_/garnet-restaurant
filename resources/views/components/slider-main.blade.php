
<div class="eltdf-row-grid-section-wrapper eltdf-content-aligment-center" id="slider-main">
    <div class="eltdf-row-grid-section">
        <div class="vc_row wpb_row vc_row-fluid">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper" id="slider-image">
                        <p class="rs-p-wp-fix"></p>
                        <rs-module-wrap id="rev_slider_1_1_wrapper"
                                        data-source="gallery"
                                        style="background:transparent;padding:0;">
                            <rs-module id="rev_slider_1_1" style="display:none;"
                                       data-version="6.1.5">
                                <rs-slides>
                                    <rs-slide data-key="rs-8" data-title="Slide"
                                              data-anim="ei:d;eo:d;s:d;r:0;t:fade;sl:d;">
                                        <img src="/images/main-h-new-rev-img-1.jpg"
                                             alt="d" title="main-h-new-rev-img-1"
                                             width="1920" height="1100"
                                             data-parallax="5" class="rev-slidebg"
                                             data-no-retina>
                                        <rs-layer id="slider-1-slide-8-layer-0"
                                                  class="farsi"
                                                  data-type="text" data-color="#c9ab81"
                                                  data-xy="x:c;xo:-270px,-277px,-141px,-68px;y:m;yo:-43px,-34px,-148px,-113px;"
                                                  data-text="w:normal;s:120,75,75,42;l:145,120,120,50;ls:28px,25px,25px,12px;a:center;"
                                                  data-dim="w:264px,264px,264px,156px;"
                                                  data-basealign="slide"
                                                  data-rsp_o="off" data-rsp_bd="off"
                                                  data-frame_0="sX:0.5;sY:0.9;"
                                                  data-frame_1="e:Power3.easeInOut;st:260;sp:1200;sR:260;"
                                                  data-frame_999="y:50;o:0;e:Power2.easeInOut;st:w;sp:850;sR:3040;"
                                                  >
                                            {{ __('homepage.slider-main-1-left') }}
                                        </rs-layer>
                                        <rs-layer id="slider-1-slide-8-layer-1"
                                                  data-type="text" data-color="#c9ab81"
                                                  data-xy="x:c;xo:0,0,0,-10px;y:m;yo:-138px,-116px,-233px,-160px;"
                                                  data-text="w:normal;s:25,25,25,19;l:35,35,35,28;a:center;"
                                                  data-dim="w:423px,423px,423px,273px;"
                                                  data-vbility="t,t,t,f"
                                                  data-basealign="slide"
                                                  data-rsp_o="off" data-rsp_bd="off"
                                                  data-frame_0="y:50;"
                                                  data-frame_1="e:Power2.easeInOut;st:190;sp:1000;sR:190;"
                                                  data-frame_999="y:-50;o:0;st:w;sp:800;sR:3310;"
                                                  style="z-index:13;font-family:Miniver;">
                                        </rs-layer>
                                        <rs-layer id="slider-1-slide-8-layer-2"
                                                  data-type="text"
                                                  data-xy="x:c;y:m;yo:78px,66px,66px,59px;"
                                                  data-text="w:normal;s:23;l:33;fw:300;a:center;"
                                                  data-dim="w:681px,681px,598px,339px;"
                                                  data-basealign="slide"
                                                  data-rsp_o="off" data-rsp_bd="off"
                                                  data-frame_0="y:50;"
                                                  data-frame_1="e:Power2.easeInOut;st:580;sp:1300;sR:580;"
                                                  data-frame_999="y:50;o:0;e:Power2.easeInOut;st:w;sp:1000;sR:2620;"
                                                  style="z-index:9;font-family:Josefin Sans;">
                                            {{ __('homepage.slider-main-1-caption') }}
                                        </rs-layer>
                                        <rs-layer id="slider-1-slide-8-layer-3"
                                                  data-type="text"
                                                  data-xy="x:c;y:m;yo:202px,175px,189px,183px;"
                                                  data-text="w:normal;a:center;"
                                                  data-basealign="slide"
                                                  data-rsp_o="off" data-rsp_bd="off"
                                                  data-frame_0="y:50;"
                                                  data-frame_1="e:Power2.easeInOut;st:750;sp:1300;sR:750;"
                                                  data-frame_999="y:50;o:0;e:Power2.easeInOut;st:w;sp:1000;sR:2450;"
                                                  style="z-index:8;font-family:Roboto;">
                                            <a itemprop=""
                                               href="http://www.time-gr.com"
                                               target=""
                                               class="eltdf-btn eltdf-btn-large eltdf-btn-outline">
                                                <span class="eltdf-btn-text">
                                                    {{ __('homepage.slider-main-1-box') }}
                                                </span>
                                            </a>
                                        </rs-layer>
                                        <rs-layer id="slider-1-slide-8-layer-5"
                                                  data-type="text" data-color="#c9ab81"
                                                  data-xy="x:c;xo:-150px,220px,12px,10px;y:m;yo:-43px,-34px,-36px,-53px;"
                                                  data-text="w:normal;s:120,75,75,42;l:145,120,120,50;ls:12x,12px,12x,12px;a:left;"
                                                  data-dim="w:472px,472px,472px,262px;"
                                                  data-basealign="slide"
                                                  data-rsp_o="off" data-rsp_bd="off"
                                                  data-frame_0="sX:0.5;sY:0.9;"
                                                  data-frame_1="e:Power2.easeInOut;st:470;sp:1200;sR:470;"
                                                  data-frame_999="y:50;o:0;e:Power2.easeInOut;st:w;sp:1250;sR:2830;"
                                                  class="farsi">
                                            {{ __('homepage.slider-main-1-right') }}
                                        </rs-layer>
                                        <rs-layer id="slider-1-slide-8-layer-22"
                                                  data-type="image"
                                                  data-xy="x:c;xo:721px,510px,327px,327px;y:m;yo:-37px,-28px,-88px,-88px;"
                                                  data-text="w:normal;"
                                                  data-dim="w:['56px','56px','56px','56px'];h:['10px','10px','10px','10px'];"
                                                  data-basealign="slide"
                                                  data-rsp_o="off" data-rsp_bd="off"
                                                  data-frame_0="sX:0.9;sY:0.9;"
                                                  data-frame_1="st:440;sp:1000;sR:440;"
                                                  data-frame_999="o:0;st:w;sR:3060;"
                                                  style="z-index:15;"><img
                                                    src="/images/rev-img.png"
                                                    alt="c" width="56" height="10"
                                                    data-no-retina>
                                        </rs-layer>
                                        <rs-layer id="slider-1-slide-8-layer-23"
                                                  data-type="image"
                                                  data-xy="x:c;xo:-725px,-505px,-327px,-327px;y:m;yo:-37px,-28px,-88px,-88px;"
                                                  data-text="w:normal;"
                                                  data-dim="w:['56px','56px','56px','56px'];h:['10px','10px','10px','10px'];"
                                                  data-basealign="slide"
                                                  data-rsp_o="off" data-rsp_bd="off"
                                                  data-frame_0="sX:0.9;sY:0.9;"
                                                  data-frame_1="st:510;sp:1000;sR:510;"
                                                  data-frame_999="o:0;st:w;sR:2990;"
                                                  style="z-index:14;"><img
                                                    src="/images/rev-img.png"
                                                    alt="c" width="56" height="10"
                                                    data-no-retina>
                                        </rs-layer>
                                    </rs-slide>
                                    <rs-slide data-key="rs-40" data-title="Slide"
                                              data-anim="ei:d;eo:d;s:d;r:0;t:fade;sl:d;">
                                        <img src="/images/home-3-gallery-img-1.jpg"
                                             alt="d" title="main-h-new-rev-img-1"
                                             width="1920" height="1100"
                                             data-parallax="5" class="rev-slidebg"
                                             data-no-retina>
                                        <rs-layer id="slider-1-slide-8-layer-0"
                                                  class="farsi"
                                                  data-type="text" data-color="#c9ab81"
                                                  data-xy="x:c;xo:-270px,-275px,-141px,-68px;y:m;yo:-43px,-34px,-148px,-113px;"
                                                  data-text="w:normal;s:100,75,75,55;l:145,120,120,50;ls:5px,5px,5px,5px;a:center;"
                                                  data-dim="w:264px,264px,264px,156px;"
                                                  data-basealign="slide"
                                                  data-rsp_o="off" data-rsp_bd="off"
                                                  data-frame_0="sX:0.5;sY:0.9;"
                                                  data-frame_1="e:Power3.easeInOut;st:260;sp:1200;sR:260;"
                                                  data-frame_999="y:50;o:0;e:Power2.easeInOut;st:w;sp:850;sR:3040;"
                                                  class="farsi">
                                            {{ __('homepage.slider-main-2-left') }}
                                        </rs-layer>
                                        <rs-layer id="slider-1-slide-40-layer-2"
                                                  data-type="text"
                                                  data-xy="x:c;y:m;yo:78px,66px,66px,59px;"
                                                  data-text="w:normal;s:23;l:33;fw:300;a:center;"
                                                  data-dim="w:743px,743px,598px,301px;"
                                                  data-basealign="slide"
                                                  data-rsp_o="off" data-rsp_bd="off"
                                                  data-frame_0="y:50;"
                                                  data-frame_1="e:Power2.easeInOut;st:580;sp:1300;sR:580;"
                                                  data-frame_999="y:50;o:0;e:Power2.easeInOut;st:w;sp:1000;sR:2620;"
                                                  style="z-index:9;font-family:Josefin Sans;">
                                            {{ __('homepage.slider-main-2-caption') }}
                                        </rs-layer>
                                        <rs-layer id="slider-1-slide-40-layer-3"
                                                  data-type="text"
                                                  data-xy="x:c;y:m;yo:202px,175px,189px,183px;"
                                                  data-text="w:normal;a:center;"
                                                  data-basealign="slide"
                                                  data-rsp_o="off" data-rsp_bd="off"
                                                  data-frame_0="y:50;"
                                                  data-frame_1="e:Power2.easeInOut;st:750;sp:1300;sR:750;"
                                                  data-frame_999="y:50;o:0;e:Power2.easeInOut;st:w;sp:1000;sR:2450;"
                                                  style="z-index:8;font-family:Roboto;">
                                            <a itemprop="url"
                                               href=""
                                               target="_self"
                                               class="eltdf-btn eltdf-btn-large eltdf-btn-outline">
                                                  <span class="eltdf-btn-text">
                                                    {{ __('homepage.slider-main-2-box') }}
                                                </span>
                                            </a>
                                        </rs-layer>
                                        <rs-layer id="slider-1-slide-8-layer-5"
                                                  class="farsi"
                                                  data-type="text" data-color="#c9ab81"
                                                  data-xy="x:c;xo:-150px,246px,12px,10px;y:m;yo:-43px,-34px,-36px,-53px;"
                                                  data-text="w:normal;s:100,75,75,55;l:145,120,120,50;ls:5px,5px,5px,5px;a:left;"
                                                  data-dim="w:472px,472px,472px,262px;"
                                                  data-basealign="slide"
                                                  data-rsp_o="off" data-rsp_bd="off"
                                                  data-frame_0="sX:0.5;sY:0.9;"
                                                  data-frame_1="e:Power2.easeInOut;st:470;sp:1200;sR:470;"
                                                  data-frame_999="y:50;o:0;e:Power2.easeInOut;st:w;sp:1250;sR:2830;"
                                                  >
                                            {{ __('homepage.slider-main-2-right') }}
                                        </rs-layer>
                                        <rs-layer id="slider-1-slide-40-layer-22"
                                                  data-type="image"
                                                  data-xy="x:c;xo:714px,500px,327px,327px;y:m;yo:-37px,-28px,-88px,-88px;"
                                                  data-text="w:normal;"
                                                  data-dim="w:['56px','56px','56px','56px'];h:['10px','10px','10px','10px'];"
                                                  data-basealign="slide"
                                                  data-rsp_o="off" data-rsp_bd="off"
                                                  data-frame_0="sX:0.9;sY:0.9;"
                                                  data-frame_1="st:440;sp:1000;sR:440;"
                                                  data-frame_999="o:0;st:w;sR:3060;"
                                                  style="z-index:15;"><img
                                                    src="/images/rev-img.png"
                                                    alt="c" width="56" height="10"
                                                    data-no-retina>
                                        </rs-layer>
                                        <rs-layer id="slider-1-slide-40-layer-23"
                                                  data-type="image"
                                                  data-xy="x:c;xo:-712px,-468px,-327px,-327px;y:m;yo:-37px,-28px,-88px,-88px;"
                                                  data-text="w:normal;"
                                                  data-dim="w:['56px','56px','56px','56px'];h:['10px','10px','10px','10px'];"
                                                  data-basealign="slide"
                                                  data-rsp_o="off" data-rsp_bd="off"
                                                  data-frame_0="sX:0.9;sY:0.9;"
                                                  data-frame_1="st:510;sp:1000;sR:510;"
                                                  data-frame_999="o:0;st:w;sR:2990;"
                                                  style="z-index:14;"><img
                                                    src="/images/rev-img.png"
                                                    alt="c" width="56" height="10"
                                                    data-no-retina>
                                        </rs-layer>
                                    </rs-slide>
                                    <rs-slide data-key="rs-41" data-title="Slide"
                                              data-anim="ei:d;eo:d;s:d;r:0;t:fade;sl:d;">
                                        <img src="/images/main-h-new-rev-img-3.jpg"
                                             alt="d" title="main-h-new-rev-img-3"
                                             width="1920" height="1100"
                                             data-parallax="5" class="rev-slidebg"
                                             data-no-retina>
                                        <rs-layer id="slider-1-slide-41-layer-0"
                                                  class="farsi"
                                                  data-type="text" data-color="#c9ab81"
                                                  data-xy="x:c;xo:-440px,-311px,-146px,-70px;y:m;yo:-43px,-34px,-148px,-113px;"
                                                  data-text="w:normal;s:120,75,75,42;l:145,120,120,50;ls:28px,25px,25px,12px;a:center;"
                                                  data-dim="w:264px,264px,264px,156px;"
                                                  data-basealign="slide"
                                                  data-rsp_o="off" data-rsp_bd="off"
                                                  data-frame_0="sX:0.9;sY:0.9;"
                                                  data-frame_1="e:Power2.easeInOut;st:260;sp:1200;sR:260;"
                                                  data-frame_999="y:50;o:0;e:Power2.easeInOut;st:w;sp:850;sR:3040;"
                                                 >
                                            {{ __('homepage.slider-main-3-left') }}
                                        </rs-layer>
                                        <rs-layer id="slider-1-slide-41-layer-2"
                                                  data-type="text"
                                                  data-xy="x:c;y:m;yo:78px,66px,66px,59px;"
                                                  data-text="w:normal;s:23;l:33;fw:300;a:center;"
                                                  data-dim="w:701px,701px,598px,298px;"
                                                  data-basealign="slide"
                                                  data-rsp_o="off" data-rsp_bd="off"
                                                  data-frame_0="y:50;"
                                                  data-frame_1="e:Power2.easeInOut;st:580;sp:1300;sR:580;"
                                                  data-frame_999="y:50;o:0;e:Power2.easeInOut;st:w;sp:1000;sR:2620;"
                                                  style="z-index:9;font-family:Josefin Sans;">
                                            {{ __('homepage.slider-main-3-caption') }}
                                        </rs-layer>
                                        <rs-layer id="slider-1-slide-41-layer-3"
                                                  data-type="text"
                                                  data-xy="x:c;y:m;yo:202px,175px,189px,183px;"
                                                  data-text="w:normal;a:center;"
                                                  data-basealign="slide"
                                                  data-rsp_o="off" data-rsp_bd="off"
                                                  data-frame_0="y:50;"
                                                  data-frame_1="e:Power2.easeInOut;st:750;sp:1300;sR:750;"
                                                  data-frame_999="y:50;o:0;e:Power2.easeInOut;st:w;sp:1000;sR:2450;"
                                                  style="z-index:8;font-family:Roboto;">
                                            <a itemprop="url"
                                               href=""
                                               target="_self"
                                               class="eltdf-btn eltdf-btn-large eltdf-btn-outline">
                                                 <span class="eltdf-btn-text">
                                                    {{ __('homepage.slider-main-3-box') }}
                                                </span>
                                            </a>
                                        </rs-layer>
                                        <rs-layer id="slider-1-slide-41-layer-4"
                                                  class="farsi"
                                                  data-type="text" data-color="#c9ab81"
                                                  data-xy="x:c;xo:-110px,-65px,120px,68px;y:m;yo:-43px,-34px,-148px,-113px;"
                                                  data-text="w:normal;s:120,75,75,42;l:145,120,120,50;ls:28px,25px,25px,12px;a:center;"
                                                  data-dim="w:417px,417px,417px,237px;"
                                                  data-basealign="slide"
                                                  data-rsp_o="off" data-rsp_bd="off"
                                                  data-frame_0="sX:0.9;sY:0.9;"
                                                  data-frame_1="e:Power2.easeInOut;st:360;sp:1200;sR:360;"
                                                  data-frame_999="y:50;o:0;e:Power2.easeInOut;st:w;sp:1050;sR:2940;"
{{--                                                  style="position:relative; right:35% !important"--}}
                                                 >
                                            {{ __('homepage.slider-main-3-center') }}
                                        </rs-layer>
                                        <rs-layer id="slider-1-slide-41-layer-5"
                                                  data-type="text" data-color="#c9ab81"
                                                  data-xy="x:c;xo:335px,267px,12px,10px;y:m;yo:-43px,-34px,-36px,-53px;"
                                                  data-text="w:normal;s:120,75,75,42;l:145,120,120,50;ls:28px,25px,25px,12px;a:center;"
                                                  data-dim="w:472px,472px,472px,262px;"
                                                  data-basealign="slide"
                                                  data-rsp_o="off" data-rsp_bd="off"
                                                  data-frame_0="sX:0.9;sY:0.9;"
                                                  data-frame_1="e:Power2.easeInOut;st:470;sp:1200;sR:470;"
                                                  data-frame_999="y:50;o:0;e:Power2.easeInOut;st:w;sp:1250;sR:2830;"
                                                  class="farsi change-slider-angle">
                                            {{ __('homepage.slider-main-3-right') }}
                                        </rs-layer>
                                        <rs-layer id="slider-1-slide-41-layer-22"
                                                  data-type="image"
                                                  data-xy="x:c;xo:644px,481px,327px,327px;y:m;yo:-37px,-28px,-88px,-88px;"
                                                  data-text="w:normal;"
                                                  data-dim="w:['56px','56px','56px','56px'];h:['10px','10px','10px','10px'];"
                                                  data-basealign="slide"
                                                  data-rsp_o="off" data-rsp_bd="off"
                                                  data-frame_0="sX:0.9;sY:0.9;"
                                                  data-frame_1="st:440;sp:1000;sR:440;"
                                                  data-frame_999="o:0;st:w;sR:3060;"
                                                  style="z-index:15;"><img
                                                    src="/images/rev-img.png"
                                                    alt="c" width="56" height="10"
                                                    data-no-retina>
                                        </rs-layer>
                                        <rs-layer id="slider-1-slide-41-layer-23"
                                                  data-type="image"
                                                  data-xy="x:c;xo:-640px,-473px,-327px,-327px;y:m;yo:-37px,-28px,-88px,-88px;"
                                                  data-text="w:normal;"
                                                  data-dim="w:['56px','56px','56px','56px'];h:['10px','10px','10px','10px'];"
                                                  data-basealign="slide"
                                                  data-rsp_o="off" data-rsp_bd="off"
                                                  data-frame_0="sX:0.9;sY:0.9;"
                                                  data-frame_1="st:510;sp:1000;sR:510;"
                                                  data-frame_999="o:0;st:w;sR:2990;"
                                                  style="z-index:14;"><img
                                                    src="/images/rev-img.png"
                                                    alt="c" width="56" height="10"
                                                    data-no-retina>
                                        </rs-layer>
                                    </rs-slide>
                                </rs-slides>
                                <rs-progress class="rs-bottom"
                                             style="visibility: hidden !important;"></rs-progress>
                            </rs-module>
                            <script type="text/javascript">
                                setREVStartSize({
                                    c: 'rev_slider_1_1',
                                    rl: [1920, 1601, 1025, 730],
                                    el: [840, 582, 900, 720],
                                    gw: [1100, 800, 700, 480],
                                    gh: [840, 582, 900, 720],
                                    layout: 'fullscreen',
                                    offsetContainer: '',
                                    offset: '',
                                    mh: "0"
                                });
                                var revapi1,
                                    tpj;
                                jQuery(function () {
                                    tpj = jQuery;
                                    if (tpj("#rev_slider_1_1").revolution == undefined) {
                                        revslider_showDoubleJqueryError("#rev_slider_1_1");
                                    } else {
                                        revapi1 = tpj("#rev_slider_1_1").show().revolution({
                                            jsFileLocation: "/js/subsidiary/revslider.js",
                                            sliderLayout: "fullscreen",
                                            duration: "4500ms",
                                            visibilityLevels: "1920,1601,1025,730",
                                            gridwidth: "1100,800,700,480",
                                            gridheight: "840,582,900,720",
                                            minHeight: "",
                                            editorheight: "840,582,900,720",
                                            responsiveLevels: "1920,1601,1025,730",
                                            disableProgressBar: "on",
                                            navigation: {
                                                onHoverStop: false,
                                                arrows: {
                                                    enable: true,
                                                    tmp: "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" width=\"25.109px\" height=\"34.906px\" viewBox=\"0 0 25.109 34.906\" enable-background=\"new 0 0 25.109 34.906\" xml:space=\"preserve\">                    <polyline fill=\"none\" stroke=\"currentColor\" stroke-miterlimit=\"10\" points=\"24.67,34.59 11.653,17.464 24.67,0.338 \"/>                    <polyline fill=\"none\" class=\"eltdf-popout\" stroke=\"currentColor\" stroke-miterlimit=\"10\" points=\"13.688,34.59 0.671,17.464 13.688,0.338 \"/></svg>",
                                                    style: "newnavclass_1",
                                                    hide_onmobile: true,
                                                    hide_under: "778px",
                                                    left: {
                                                        h_offset: 30
                                                    },
                                                    right: {
                                                        h_offset: 30
                                                    }
                                                },
                                                bullets: {
                                                    enable: true,
                                                    tmp: "<button class=\"dot\"><span></span></button>",
                                                    style: "newnavclass_2",
                                                    hide_onmobile: true,
                                                    hide_under: "678px",
                                                    v_offset: 50,
                                                    space: 45
                                                }
                                            },
                                            parallax: {
                                                levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 51, 30],
                                                type: "scroll",
                                                origo: "slideCenter",
                                                speed: 0
                                            },
                                            fallbacks: {
                                                allowHTML5AutoPlayOnAndroid: true
                                            },
                                        });
                                    }

                                });
                            </script>
                            <script>
                                var htmlDivCss = unescape(".tp-leftarrow%2C%0A.tp-rightarrow%20%7B%0A%20%20color%3A%20%23c9ab81%3B%0A%20%20background%3A%20none%0A%7D%0A%0A.tp-leftarrow%3Abefore%2C%0A.tp-rightarrow%3Abefore%20%7B%0A%20%20display%3A%20none%0A%7D%0A%0A.tp-leftarrow%20svg%2C%0A.tp-rightarrow%20svg%20%7B%0A%20%20max-height%3A%20100%25%3B%0A%20%20width%3A%20auto%3B%0A%20%20stroke-width%3A%201px%3B%0A%20%20transform%3A%20translateX%280%29%3B%0A%7D%0A%0A.tp-rightarrow%20svg%20%7B%0A%20transform%3A%20translateX%280%29%20scaleX%28-1%29%0A%7D%0A%0A.tp-leftarrow%20svg%20.eltdf-popout%20%7B%0A%20%20%20%20stroke-dashoffset%3A%2043%3B%0A%20%20%20%20stroke-dasharray%3A%2043%3B%0A%20%20%20%20stroke-linejoin%3A%20round%3B%0A%20%20%20%20stroke-miterlimit%3A%2010%3B%0A%20%20%20%20stroke-linecap%3A%20butt%3B%0A%20%20%20%20transform%3A%20rotateX%28180deg%29%3B%0A%20%20%20%20transform-origin%3A%20center%20center%3B%0A%20%20%20%20transition%3A%20stroke-dashoffset%20.5s%20cubic-bezier%280.48%2C%200.57%2C%200.33%2C%200.89%29%3B%0A%7D%0A%0A.tp-leftarrow%3Ahover%20svg%20.eltdf-popout%2C%0A.tp-rightarrow%3Ahover%20svg%20.eltdf-popout%20%7B%0A%20%20%20%20transform%3A%20translateX%280%29%20rotateX%28180deg%29%3B%0A%20%20%20%20stroke-dashoffset%3A%200%3B%0A%20%20%20%20stroke-dasharray%3A%2043%3B%0A%20%20%20%20transition%3A%20stroke-dashoffset%20.5s%20cubic-bezier%280.48%2C%200.57%2C%200.33%2C%200.89%29%3B%0A%7D%0A%0A.tp-leftarrow%3Ahover%2C%0A.tp-rightarrow%3Ahover%20%7B%0A%20%20%09background%3A%20none%0A%7D%0A%0A%0A.tp-rightarrow%20svg%20.eltdf-popout%20%7B%0A%20%20%20%20stroke-dashoffset%3A%2043%3B%0A%20%20%20%20stroke-dasharray%3A%2043%3B%0A%20%20%20%20stroke-linejoin%3A%20round%3B%0A%20%20%20%20stroke-miterlimit%3A%2010%3B%0A%20%20%20%20stroke-linecap%3A%20butt%3B%0A%20%20%20%20transform%3A%20rotateX%28180deg%29%3B%0A%20%20%20%20transform-origin%3A%20center%20center%3B%0A%20%20%20%20transition%3A%20stroke-dashoffset%20.5s%20cubic-bezier%280.48%2C%200.57%2C%200.33%2C%200.89%29%3B%0A%7D%0A%0A.tp-rightarrow%3Ahover%20svg%20%7B%0A%20%20%09transform%3A%20scaleX%28-1%29%3B%0A%7D%0A.tp-bullets%20%7B%0A%20%20text-align%3A%20center%3B%0A%20%20margin%3A%200%3B%0A%20%20counter-reset%3A%20dots%3B%0A%20%20box-sizing%3A%20content-box%3B%0A%7D%0A.tp-bullet%20%7B%0A%20%20width%3Aauto%3B%0A%20%20height%3A%20auto%3B%0A%7D%0A%0A.tp-bullet%2C%0A.tp-bullet%3Ahover%2C%0A.tp-bullet.selected%2C%0A%23rb_tlw%20button%20%7B%0A%20%20background%3A%20none%3B%0A%20%20box-sizing%3A%20content-box%3B%0A%7D%0A%0A%23rb_tlw%20button%3Ahover%20%7B%0A%20%20background%3Anone%0A%7D%0A%0A.tp-bullets%20.dot%20%7B%0A%20%20position%3A%20relative%3B%0A%20%20bottom%3A%200%3B%0A%20%20display%3A%20inline-block%3B%0A%20%20vertical-align%3A%20middle%3B%0A%20%20padding%3A%200%20%21important%3B%0A%20%20margin%3A%200%3B%0A%20%20background%3A%20none%3B%0A%20%20border%3A%200%3B%0A%20%20border-radius%3A%200%3B%0A%20%20outline%3A%20none%3B%0A%20%20-webkit-appearance%3A%20none%3B%0A%20%20overflow%3A%20hidden%3B%0A%20%20cursor%3Apointer%3B%0A%7D%0A%0A.tp-bullets%20.dot%20span%20%7B%0A%20%20display%3A%20inline-block%3B%0A%20%20vertical-align%3A%20middle%3B%0A%20%20width%3A%2012px%3B%0A%20%20height%3A%202.2em%3B%0A%20%20background-color%3A%20transparent%3B%0A%20%20border%3A%20none%3B%0A%20%20padding-top%3A%205px%3B%0A%20%20color%3A%20%23c9ab81%20%21important%3B%0A%7D%0A%0A.eltdf-safari%20.tp-bullets%20.dot%20span%20%7B%0A%20%20height%3A%203em%3B%0A%20%20padding-top%3A%206px%3B%0A%7D%0A%0A.tp-bullets%20.dot%20span%3Abefore%20%7B%0A%20%20content%3A%20counter%28dots%29%3B%0A%20%20counter-increment%3A%20dots%3B%0A%20%20position%3A%20absolute%3B%0A%20%20top%3A%20calc%28100%25%20-%201.5em%29%3B%0A%20%20left%3A%202px%3B%0A%20%20font-family%3A%20%27caviar_dreamsbold%27%2C%20sans-serif%3B%0A%20%20font-size%3A%2012px%3B%0A%20%20font-weight%3A%20700%3B%0A%20%20letter-spacing%3A%20.23em%3B%0A%20%20color%3A%20inherit%3B%0A%20%20transform%3A%20translateY%280%29%3B%0A%20%20transition%3A%20transform%20.2s%20ease-out%20.2s%2C%20color%20.2s%20ease-in-out%3B%0A%7D%0A%0A.tp-bullets%20.dot%20span%3Aafter%20%7B%0A%20%20content%3A%20%27%27%3B%0A%20%20display%3A%20block%3B%0A%20%20width%3A%20100%25%3B%0A%20%20height%3A%203px%3B%0A%20%20border-top%3A%201px%20solid%20%23c9ab81%3B%0A%20%20border-bottom%3A%201px%20solid%20%20%23c9ab81%3B%0A%20%20position%3A%20relative%3B%0A%20%20left%3A%20-1px%3B%0A%20%20bottom%3A%20-24px%3B%0A%20%20transform%3A%20translateY%28100%25%29%3B%0A%20%20transition%3A%20transform%20.2s%20ease-out%20.1s%3B%0A%7D%0A%0A.tp-bullets%20.dot%3Ahover%20span%3Abefore%20%7B%0A%20%20color%3A%20%23e3c59b%3B%0A%7D%0A%0A.tp-bullet.selected%20.dot%3Ahover%20span%3Abefore%20%7B%0A%20%20color%3A%20inherit%3B%0A%7D%0A%0A.tp-bullet.selected%20.dot%20span%3Abefore%20%7B%0A%20%20transform%3A%20translateY%28-1em%29%3B%0A%20%20transition%3A%20transform%20.2s%20ease-in%3B%0A%7D%0A%0A.tp-bullet.selected%20.dot%20span%3Aafter%20%7B%0A%20%20transform%3A%20translateY%28-6px%29%3B%0A%20%20transition%3A%20transform%20.2s%20ease-in%20.1s%3B%0A%7D%0A");
                                var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                                if (htmlDiv) {
                                    htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                } else {
                                    var htmlDiv = document.createElement('div');
                                    htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                                    document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                                }
                            </script>
                            <script>
                                var htmlDivCss = unescape("%0A%0A%0A%0A%0A%0A");
                                var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                                if (htmlDiv) {
                                    htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                } else {
                                    var htmlDiv = document.createElement('div');
                                    htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                                    document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                                }
                            </script>
                        </rs-module-wrap>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

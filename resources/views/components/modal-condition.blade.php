@if (Session::has('message'))
    @if(Session::has('message') == 'success')
        <x-modal mess="{{ __('homepage.modal-message-y') }}"/>
    @elseif(Session::has('message') == 'error')
        <x-modal mess="{{ __('homepage.modal-message-n') }}"/>
    @endif
@endif
@if (Session::has('booking'))
    @if(Session::has('booking') == 'success')
        <x-modal mess="{{ __('homepage.modal-booking-y') }}"/>
    @elseif(Session::has('booking') == 'error')
        <x-modal mess="{{ __('homepage.modal-booking-n') }}"/>
    @endif
@endif
@if (Session::has('reservation'))
    @if(Session::has('reservation') == 'success')
        <x-modal mess="{{ __('homepage.modal-reserve-del-y') }}"/>
    @elseif(Session::has('reservation') == 'error')
        <x-modal mess="{{ __('homepage.modal-reserve-del-n') }}"/>
    @endif
@endif
@if (Session::has('question'))
    @if(Session::has('question') == 'success')
        <x-modal mess="{{ __('homepage.modal-q-del-y') }}"/>
    @elseif(Session::has('question') == 'error')
        <x-modal mess="{{ __('homepage.modal-q-del-n') }}"/>
    @endif
@endif
@if( count($errors))
    <x-modal err="{{ $errors }}"/>
@endif

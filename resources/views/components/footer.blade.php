<footer class="eltdf-page-footer" id="footer" >
    <div class="eltdf-footer-top-holder" id="contact" >
        <div class="eltdf-footer-top-inner eltdf-grid" style="padding-bottom: 0; margin-bottom: 0">
            <div class="eltdf-grid-lines-holder eltdf-grid-columns-5">
                <div class="eltdf-grid-line eltdf-grid-column-1"></div>
                <div class="eltdf-grid-line eltdf-grid-column-2"></div>
                <div class="eltdf-grid-line eltdf-grid-column-3"></div>
                <div class="eltdf-grid-line eltdf-grid-column-4"></div>
                <div class="eltdf-grid-line eltdf-grid-column-5"></div>
            </div>
            {{--footnote--}}
            <div class="eltdf-grid-row eltdf-footer-top-alignment-center">
                <div id="text-2" class="widget eltdf-footer-column-1 widget_text">
                    <div class="textwidget"><p style="line-height: 14px; margin-top:-60px">
                            <img  width="120"
                                  src="/images/logo-g.png"
                                  class="image wp-image-75  attachment-full size-full logo-garnet-f"
                                  alt="u"
                                  loading="lazy"
                            />
                            {{ __('homepage.contact-name') }}
                        </p>
                    </div>
                </div>
                <div id="text-3" class="widget eltdf-footer-column-1 widget_text">
                    <div class="textwidget">
                        <p style="line-height: 30px;">
                            <a href="https://maps.google.com/maps?width=520&amp;height=400&amp;hl=en&amp;q=Tehran%20Province,%20Tehran,%20Mehr%20Mohammadi%20Rd,%20Iran%20Tehran+(Garnet%20Luxury%20Cafe%20)&amp;t=&amp;z=15&amp;ie=UTF8&amp;iwloc=B&amp"
                               target="_blank">{{ __('homepage.contact-address') }} </a><br>
                            <a href="tel:021-49927"> {{ __('homepage.contact-tel') }} </a> -
                            <a href="tel:09120499277" title="Management"> {{ __('homepage.contact-tel2') }} </a><br>
                            <a href="mailto:info@garnetrestaurant.com">
                                {{ __('homepage.contact-email') }}
                            </a>
                        </p>
                    </div>
                </div>
                <div id="text-5" class="widget eltdf-footer-column-1 widget_text">
                    <div class="textwidget">
                        <p style="line-height: 14px; color:#C9AB81">
                            {{ __('homepage.contact-open') }}
                        </p>
                    </div>
                </div>
                <div class="widget eltdf-separator-widget">
                    <div class="eltdf-separator-holder clearfix  eltdf-separator-center eltdf-separator-normal">
                        <div class="eltdf-separator"
                             style="border-style: solid;width: 0px;border-bottom-width: 2px;margin-top: 0px">
                        </div>
                    </div>
                </div>
            </div>
            <br>
            {{--contact & map--}}
            <div class="eltdf-row-grid-section-wrapper eltdf-content-aligment-center">
                <div class="eltdf-row-grid-section">
                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1573659308492">
                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div class="eltdf-section-title-holder    eltdf-st-decor-animation"
                                         style="text-align: center">
                                        <div class="eltdf-st-inner"><span class="eltdf-st-tagline">{{ __('homepage.form-head') }} </span>
                                            <div class="eltdf-st-title-holder">
                                                <div class="decor">
                                                    <svg height="9.146"
                                                         width="41.125" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M40.881 8.576L20.562.591.244 8.576" fill="none"
                                                              stroke="#9C7C57"
                                                              stroke-miterlimit="10"/>
                                                        <path d="M40.881.591L20.562 8.576.243.591" fill="none"
                                                              stroke="#9C7C57"
                                                              stroke-miterlimit="10"/>
                                                    </svg>
                                                </div>
                                                <h2 class="eltdf-st-title">
                                                    {{ __('homepage.form-title') }}
                                                </h2>
                                                <div class="decor">
                                                    <svg height="9.146"
                                                         width="41.125" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M40.881 8.576L20.562.591.244 8.576" fill="none"
                                                              stroke="#9C7C57"
                                                              stroke-miterlimit="10"/>
                                                        <path d="M40.881.591L20.562 8.576.243.591" fill="none"
                                                              stroke="#9C7C57"
                                                              stroke-miterlimit="10"/>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="vc_empty_space" style="height: 55px">
                                        <span class="vc_empty_space_inner"></span>
                                    </div>
                                    <div dir="ltr" lang="en-US">
                                        <form action="{{ route('questions' )}}"
                                              class="cf7_custom_style_2 change-dir" method="post">
                                            @csrf()
                                            <div class="">
                                                <span class="wpcf7-form-control-wrap your-name pad-20">
                                                    <input class="wpcf7-text"
                                                           required
                                                           name="name"
                                                           placeholder="{{ __('homepage.form-name') }}"
                                                           size="40" type="text"/>
                                                </span>
                                                <span class="wpcf7-form-control-wrap wpcf7-phone">
                                                     <input class="wpcf7-phone"
                                                            name="phone"
                                                            size="40" type="number"/>
                                                </span><br/>
                                                <span class="wpcf7-form-control-wrap your-email">
                                                    <input class="wpcf7-text wpcf7-email"
                                                           required
                                                           name="email"
                                                           placeholder="{{ __('homepage.form-email') }}"
                                                           size="40" type="email"/>
                                                </span><br/>
                                                <span class="wpcf7-form-control-wrap your-message">
                                                    <textarea
                                                            required
                                                            class="wpcf7-form-control wpcf7-textarea" cols="40"
                                                            name="message"
                                                            rows="10">{{ __('homepage.form-message') }}</textarea></span>
                                                <div class="qodef-cf7-contact-btn-holder">
                                                    <button class="wpcf7-form-control wpcf7-submit eltdf-btn eltdf-btn-medium eltdf-btn-outline"
                                                            type="submit">
                                                        <span class="eltdf-btn-text">{{ __('homepage.form-box') }}</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <x-google-map/>
                    </div>
                </div>
            </div>
            <br>
            {{--separator --}}
            <div class="vc_row wpb_row vc_row-fluid">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="eltdf-separator-holder clearfix  eltdf-separator-center eltdf-separator-full-width">
                                <div class="eltdf-separator"
                                     style="border-color: #715b3e;border-bottom-width: 1px;margin-top: 0px;margin-bottom: 0px"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            {{--footnote--}}
            <div class="eltdf-grid-row eltdf-footer-top-alignment-center">
                <div class="eltdf-column-content eltdf-grid-col-12">
                    <div id="media_image-2" class="widget eltdf-footer-column-1 widget_media_image"><a
                                href="index.html"><img  width="200"
                                                       src="/images/Garnet-gold.jpg"
                                                       class="image wp-image-75  attachment-full size-full"
                                                       alt="u"
                                                       loading="lazy"
                                                       style="max-width: 100%; height: auto;"/></a>
                    </div>
                    <div id="text-5" class="widget eltdf-footer-column-1 widget_text">
                        <div class="textwidget">
                            <p>
                                {{ __('homepage.contact-line') }}
                            </p>
                        </div>
                    </div>
                    <div id="nav_menu-2" class="widget eltdf-footer-column-1 widget_nav_menu">
                        <div class="menu-footer-menu-container">
                            <ul id="menu-footer-menu" class="menu">
                                <li id="menu-item-70"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-70">
                                    <a target="_blank" rel="noopener noreferrer"
                                       href="https://www.instagram.com/garnet_restaurant" title="{{ __('homepage.IG') }}">
                                        <i class="eltdf-icon-ion-icon ion-social-instagram-outline eltdf-author-social-instagram eltdf-author-social-icon "
                                           style="font-size: 37px;color: #c9ab81;"></i>
                                    </a>
                                </li>
                            </ul>
                                <br>
                                <p class="footer-rights">
                                    {{ __('homepage.contact-rights') }}  <br>  {{ __('homepage.reservation-date') }}
                                </p>

                                <ul id="menu-footer-menu" class="menu">
                                     <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-71 developer">
                                         <a target="_blank" rel="noopener noreferrer" class="smaller developer" href="http://time-gr.com/cv/">
                                             {{ __('homepage.dev') }}
                                         </a>
                                     </li>
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

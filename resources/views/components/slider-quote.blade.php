<div class="vc_row wpb_row vc_row-fluid" id="slider-quote">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner">
            <div class="wpb_wrapper">
                <div class="eltdf-elements-holder   eltdf-two-columns  eltdf-responsive-mode-768  fifty-fifty">
                    <div class="eltdf-eh-item    " style="background-color: #0f1d22"
                         data-item-class="eltdf-eh-custom-7151"
                         data-1400-1600="23% 0% 23% 15%" data-1025-1399="23% 0% 23% 15%"
                         data-769-1024="23% 0%" data-681-768="23% 0%" data-680="23% 0%">
                        <div class="eltdf-eh-item-inner">
                            <div class="eltdf-eh-item-content eltdf-eh-custom-7151"
                                 style="padding: 23% 0 23% 4%">
                                <div class="eltdf-testimonials-holder clearfix eltdf-testimonials-standard ">
                                    <div class="eltdf-testimonials-mark"></div>
                                    <div class="eltdf-testimonials eltdf-owl-slider"
                                         data-number-of-items="1" data-enable-loop="yes"
                                         data-enable-autoplay="yes"
                                         data-slider-speed="5000"
                                         data-slider-speed-animation="600"
                                         data-enable-navigation="no"
                                         data-enable-pagination="yes">
                                        <div class="eltdf-testimonial-content"
                                             id="eltdf-testimonials-160">
                                            <div class="eltdf-testimonial-text-holder">
                                                <p class="eltdf-testimonial-text">
                                                    {{ __('homepage.quote-1') }}
                                                </p>
                                                <h5 class="eltdf-testimonial-author">
                                                    <span class="eltdf-testimonials-author-name">
                                                    {{ __('homepage.quote-1-name') }}
                                                    </span>
                                                </h5>
                                                <p class="eltdf-testimonials-author-job">
                                                    {{ __('homepage.quote-1-title') }}

                                                </p>
                                            </div>
                                        </div>
                                        <div class="eltdf-testimonial-content"
                                             id="eltdf-testimonials-98">
                                            <div class="eltdf-testimonial-text-holder">
                                                <p class="eltdf-testimonial-text">
                                                    {{ __('homepage.quote-2') }}
                                                </p>
                                                <h5 class="eltdf-testimonial-author">
                                                    <span class="eltdf-testimonials-author-name">
                                                      {{ __('homepage.quote-2-name') }}
                                                    </span>
                                                </h5>
                                                <p class="eltdf-testimonials-author-job">
                                                    {{ __('homepage.quote-2-title') }}
                                                </p>
                                            </div>
                                        </div>
{{--                                        <div class="eltdf-testimonial-content"--}}
{{--                                             id="eltdf-testimonials-36">--}}
{{--                                            <div class="eltdf-testimonial-text-holder">--}}
{{--                                                <p class="eltdf-testimonial-text">--}}
{{--                                                    {{ __('homepage.quote-3') }}--}}
{{--                                                </p>--}}
{{--                                                <h5 class="eltdf-testimonial-author">--}}
{{--                                                    <span class="eltdf-testimonials-author-name">--}}
{{--                                                 {{ __('homepage.quote-3-name') }}--}}
{{--                                                    </span>--}}
{{--                                                </h5>--}}
{{--                                                <p class="eltdf-testimonials-author-job">--}}
{{--                                                    {{ __('homepage.quote-3-title') }}--}}
{{--                                                </p>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="eltdf-eh-item    "
                         style="background-image: url('/images/main-img-4.jpg')"
                         data-item-class="eltdf-eh-custom-8690" data-681-768="40% 0"
                         data-680="60% 0">
                        <div class="eltdf-eh-item-inner">
                            <div class="eltdf-eh-item-content eltdf-eh-custom-8690">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

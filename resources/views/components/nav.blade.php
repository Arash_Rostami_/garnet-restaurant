<header class="eltdf-page-header" id="nav">
    <!--<div class="eltdf-header-double-grid-line-one"></div><div class="eltdf-header-double-grid-line-two"></div>-->
    <div class="eltdf-menu-area eltdf-menu-center">
        <div class="eltdf-vertical-align-containers">
            <div class="eltdf-position-left">
                <div class="eltdf-position-left-inner">
                    <img  width="120"
                          src="/images/logo-g.png"
                          class="image wp-image-75  attachment-full size-full"
                          alt="logo"
                          style="position:relative;height: auto; top:10px"/>
                </div>
            </div>
            <div class="eltdf-position-center">
                <div class="eltdf-position-center-inner">
                    <nav class="eltdf-main-menu eltdf-drop-down eltdf-default-nav">
                        <ul id="menu-main-menu" class="clearfix" >
                            <li id="nav-menu-item-13"
                                class="float-direction menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children eltdf-active-item has_sub narrow">
                                <a href="#" class=" current "><span class="item_outer"><span class="item_text" id="#">{{ __('homepage.menu-1') }}</span></span></a>
                                <div class="second">
                                    <div class="inner"></div>
                                </div>
                            </li>
                            <li id="nav-menu-item-15"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow">
                                <a href="#story" class=""><span class="item_outer"><span
                                                class="item_text">{{ __('homepage.menu-2') }}</span></span></a>
                                <div class="second">
                                    <div class="inner">

                                    </div>
                                </div>
                            </li>
                            <li id="nav-menu-item-16"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow">
                                <a href="#gallery" class=""><span class="item_outer"><span
                                                class="item_text">{{ __('homepage.menu-3') }}</span></span></a>
                                <div class="second">
                                    <div class="inner">
                                    </div>
                                </div>
                            </li>
                            <li id="nav-menu-item-17"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow">
                                <a href="https://garnet.menew.pw/" class="" target="_blank"><span class="item_outer"><span
                                                class="item_text">{{ __('homepage.menu-4') }}</span></span></a>
                                <div class="second">
                                    <div class="inner">
                                    </div>
                                </div>
                            </li>
                            <li id="nav-menu-item-18"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow">
                                <a href="#reservation" class=""><span class="item_outer"><span
                                                class="item_text">{{ __('homepage.menu-5') }}</span></span></a>
                                <div class="second">
                                    <div class="inner">
                                    </div>
                                </div>
                            </li>
                            <li id="nav-menu-item-19"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow">
                                <a href="#contact" class=""><span class="item_outer"><span
                                                class="item_text">{{ __('homepage.menu-6') }}</span></span></a>
                                <div class="second">
                                    <div class="inner">
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="eltdf-position-right"  style="position:relative;top:10px;">
                <div class="eltdf-position-right-inner">
                    <a class="eltdf-side-menu-button-opener eltdf-icon-has-hover eltdf-side-menu-button-opener-predefined"
                       href="javascript:void(0)">
<span class="eltdf-side-menu-icon">
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 37 25.2" class="eltdf-menu-opener">
    <line y1="7.6" x2="24" y2="7.6"/><line x1="4.1" y1="0.5" x2="28.1" y2="0.5"/>
    <line x1="10.1" y1="24.6" x2="34.1" y2="24.6"/><line x1="13" y1="17.6" x2="37" y2="17.6"/>
</svg>
</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>

<div id="philosophy">
    <div class="eltdf-row-grid-section-wrapper ">
        <div class="eltdf-row-grid-section">
            <div class="vc_row wpb_row vc_row-fluid vc_custom_1577721744350">
                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-7 vc_col-md-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div
                                class="eltdf-elements-holder   eltdf-two-columns  eltdf-responsive-mode-680  fifty-fifty">
                                <div class="eltdf-eh-item    "
                                     data-item-class="eltdf-eh-custom-8997"
                                     data-1400-1600="0 23px 0 0" data-1025-1399="0 23px 0 0"
                                     data-680="0 0 50px 0">
                                    <div class="eltdf-eh-item-inner">
                                        <div class="eltdf-eh-item-content eltdf-eh-custom-8997"
                                             style="padding: 0 27px 0 0">
                                            <div class="eltdf-single-image-holder     eltdf-image-appear-from-top">
                                                <div class="eltdf-si-inner">
                                                    <img width="800" height="1275"
                                                         src="/images/Meet-the-chef-img.jpg"
                                                         class="attachment-full size-full"
                                                         alt="g" loading="lazy"
                                                         srcset="/images/Meet-the-chef-img.jpg 800w, images/Meet-the-chef-img.jpg 600w, images/Meet-the-chef-img.jpg 188w, images/Meet-the-chef-img.jpg 768w, images/Meet-the-chef-img.jpg 643w"
                                                         sizes="(max-width: 750px) 100vw, 800px"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="eltdf-eh-item    "
                                     data-item-class="eltdf-eh-custom-5259"
                                     data-1400-1600="0 0 0 23px" data-1025-1399="0 0 0 23px"
                                     data-680="0 0">
                                    <div class="eltdf-eh-item-inner">
                                        <div class="eltdf-eh-item-content eltdf-eh-custom-5259"
                                             style="padding: 0 0 0 27px">
                                            <div
                                                class="eltdf-single-image-holder eltdf-main-home-delayed-image    eltdf-image-appear-from-top">
                                                <div class="eltdf-si-inner">
                                                    <img width="800" height="1275"
                                                         src="/images/main-img-8.jpg"
                                                         class="attachment-full size-full"
                                                         alt="h" loading="lazy"
                                                         srcset="/images/main-img-8.jpg 800w, images/main-img-8-600x956.jpg 600w, images/main-img-8-188x300.jpg 188w, images/main-img-8-768x1224.jpg 768w, images/main-img-8-643x1024.jpg 643w"
                                                         sizes="(max-width: 800px) 100vw, 800px"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-5 vc_col-md-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="eltdf-elements-holder   eltdf-one-column  eltdf-responsive-mode-768  ">
                                <div class="eltdf-eh-item    "
                                     data-item-class="eltdf-eh-custom-6137"
                                     data-1400-1600="13% 0% 0 9%"
                                     data-1025-1399="9% 0% 0 5%"
                                     data-769-1024="80px 11% 0 11%"
                                     data-681-768="80px 0 0 0" data-680="80px 0 0 0">
                                    <div class="eltdf-eh-item-inner">
                                        <div class="eltdf-eh-item-content eltdf-eh-custom-6137"
                                             style="padding: 20% 11% 0 11%">
                                            <div class="eltdf-section-title-holder    eltdf-st-decor-animation"
                                                 style="text-align: center">
                                                <div class="eltdf-st-inner">
                                                <span class="eltdf-st-tagline">
                                                 {{ __('homepage.philosophy-head') }}
                                                </span>
                                                    <div class="eltdf-st-title-holder">
                                                        <div class="decor">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="41.125"
                                                                 height="9.146">
                                                                <path fill="none"
                                                                      stroke="#9C7C57"
                                                                      stroke-miterlimit="10"
                                                                      d="M40.881 8.576L20.562.591.244 8.576"/>
                                                                <path fill="none"
                                                                      stroke="#9C7C57"
                                                                      stroke-miterlimit="10"
                                                                      d="M40.881.591L20.562 8.576.243.591"/>
                                                            </svg>
                                                        </div>
                                                        <h2 class="eltdf-st-title">
                                                            {{ __('homepage.philosophy-title') }}
                                                        </h2>
                                                        <div class="decor">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="41.125"
                                                                 height="9.146">
                                                                <path fill="none"
                                                                      stroke="#9C7C57"
                                                                      stroke-miterlimit="10"
                                                                      d="M40.881 8.576L20.562.591.244 8.576"/>
                                                                <path fill="none"
                                                                      stroke="#9C7C57"
                                                                      stroke-miterlimit="10"
                                                                      d="M40.881.591L20.562 8.576.243.591"/>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <p class="eltdf-st-text">
                                                        {{ __('homepage.philosophy-text') }}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div data-parallax-bg-image="/images/setting.jpg"
         data-parallax-bg-speed="0.5"
         class="vc_row wpb_row vc_row-fluid vc_custom_1574159245515 eltdf-parallax-row-holder eltdf-content-aligment-center">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="eltdf-video-button-holder  ">
                        <div class="eltdf-video-button-image">   </div>
                        <a class="eltdf-video-button-play pointer" onclick="playMe()">
<span class="eltdf-video-button-play-inner">
<span title="{{ __('homepage.video') }}">
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="97.094px"
     height="97.094px" viewBox="0 0 97.094 97.094" enable-background="new 0 0 97.094 97.094" xml:space="preserve">
<circle fill="none" stroke="currentColor" stroke-miterlimit="10" cx="48.558" cy="48.548" r="48"/>
<circle fill="none" class="eltdf-popout" stroke="none" stroke-miterlimit="10" cx="48.558" cy="48.548" r="41.037"/>
<polygon fill="none" stroke="currentColor" stroke-miterlimit="10" points="42.578,69.964 42.578,27.13 63.994,48.546 "/>
</svg>
</span>
</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


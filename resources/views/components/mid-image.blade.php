<div id="mid-image">
    <div class="eltdf-row-grid-section-wrapper eltdf-content-aligment-center pad-100" >
        <div class="eltdf-row-grid-section">
            <div class="vc_row wpb_row vc_row-fluid vc_custom_1572956441174">
                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-6">
                    <div class="vc_column-inner vc_custom_1571835123146">
                        <div class="wpb_wrapper">
                            <div class="eltdf-iwt clearfix  eltdf-iwt-icon-top eltdf-iwt-icon-medium">
                                <div class="eltdf-iwt-icon">
                                    <img width="73" height="78"
                                         src="/images/home-3-icon-img-1.png"
                                         class="animation attachment-full size-full" alt="g"
                                         loading="lazy"/></div>
                                <div class="eltdf-iwt-content">
                                    <h5 class="eltdf-iwt-title">
                                        <span class="eltdf-iwt-title-text">
                                         {{ __('homepage.service-1') }}

                                        </span>
                                    </h5>
                                    <p class="eltdf-iwt-text">
                                       {{ __('homepage.service-1-text') }}
                                    </p>
                                </div>
                            </div>
                            <div class="vc_empty_space" style="height: 50px"><span
                                        class="vc_empty_space_inner"></span></div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-6">
                    <div class="vc_column-inner vc_custom_1571835132133">
                        <div class="wpb_wrapper">
                            <div class="eltdf-iwt clearfix  eltdf-iwt-icon-top eltdf-iwt-icon-medium">
                                <div class="eltdf-iwt-icon">
                                    <img width="111" height="78"
                                         src="/images/home-3-icon-img-2.png"
                                         class="animation attachment-full size-full" alt="d"
                                         loading="lazy"/></div>
                                <div class="eltdf-iwt-content">
                                    <h5 class="eltdf-iwt-title">
                                        <span class="eltdf-iwt-title-text">
                                           {{ __('homepage.service-2') }}

                                        </span>
                                    </h5>
                                    <p class="eltdf-iwt-text">
                                        {{ __('homepage.service-2-text') }}
                                    </p>
                                </div>
                            </div>
                            <div class="vc_empty_space" style="height: 50px"><span
                                        class="vc_empty_space_inner"></span></div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-6">
                    <div class="vc_column-inner vc_custom_1571835610547">
                        <div class="wpb_wrapper">
                            <div class="eltdf-iwt clearfix  eltdf-iwt-icon-top eltdf-iwt-icon-medium">
                                <div class="eltdf-iwt-icon">
                                    <img width="74" height="78"
                                         src="/images/home-3-icon-img-4.png"
                                         class="animation attachment-full size-full" alt="d"
                                         loading="lazy"/></div>
                                <div class="eltdf-iwt-content">
                                    <h5 class="eltdf-iwt-title">
                                        <span class="eltdf-iwt-title-text">
                                             {{ __('homepage.service-3') }}
                                        </span>
                                    </h5>
                                    <p class="eltdf-iwt-text">
                                        {{ __('homepage.service-3-text') }}
                                    </p>
                                </div>
                            </div>
                            <div class="vc_empty_space" style="height: 50px"><span
                                        class="vc_empty_space_inner"></span></div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-6">
                    <div class="vc_column-inner vc_custom_1571835616918">
                        <div class="wpb_wrapper">
                            <div class="eltdf-iwt clearfix  eltdf-iwt-icon-top eltdf-iwt-icon-medium">
                                <div class="eltdf-iwt-icon">
                                    <img width="57" height="78"
                                         src="/images/home-3-icon-img-5.png"
                                         class="animation attachment-full size-full" alt="d"
                                         loading="lazy"/></div>
                                <div class="eltdf-iwt-content">
                                    <h5 class="eltdf-iwt-title">
                                        <span class="eltdf-iwt-title-text">
                                                  {{ __('homepage.service-4') }}

                                        </span>
                                    </h5>
                                    <p class="eltdf-iwt-text">
                                        {{ __('homepage.service-4-text') }}
                                    </p>
                                </div>
                            </div>
                            <div class="vc_empty_space" style="height: 50px"><span
                                        class="vc_empty_space_inner"></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="eltdf-content-inner" id="gallery">
        <div class="eltdf-full-width">
            <div class="eltdf-full-width-inner">
                <div class="eltdf-grid-row">
                    <div class="eltdf-page-content-holder eltdf-grid-col-12">
                        <div class="vc_row wpb_row vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="eltdf-portfolio-slider-holder eltdf-pfs-fullscreen">
                                            <div class="eltdf-portfolio-list-holder eltdf-grid-list eltdf-portfolio-info-float eltdf-pl-gallery eltdf-three-columns eltdf-small-space eltdf-disable-bottom-space eltdf-pl-standard-float    eltdf-pl-pag-no-pagination     eltdf-pag-below-slider swiper-container "
                                                 data-type=gallery data-space-between-items=small
                                                 data-number-of-items=9 data-image-proportions=full
                                                 data-enable-fixed-proportions=no data-enable-image-shadow=no
                                                 data-category=restaurant data-orderby=date data-order=ASC
                                                 data-item-style=standard-float data-enable-title=yes
                                                 data-title-tag=h6 data-enable-category=yes
                                                 data-enable-count-images=yes data-enable-excerpt=no
                                                 data-excerpt-length=20 data-pagination-type=no-pagination
                                                 data-filter=no data-filter-order-by=name
                                                 data-enable-article-animation=no data-portfolio-slider-on=yes
                                                 data-enable-auto-width=no data-enable-mouse-scroll=yes
                                                 data-enable-loop=yes data-enable-autoplay=yes
                                                 data-slider-speed=5000 data-slider-speed-animation=600
                                                 data-enable-navigation=no data-enable-pagination=no
                                                 data-pagination-position=below-slider data-max-num-pages=1
                                                 data-next-page=2>
                                                <div class="eltdf-pl-inner eltdf-outer-space swiper-wrapper clearfix">
                                                    <article
                                                            class="eltdf-pl-item eltdf-item-space  swiper-slide post-818 portfolio-item type-portfolio-item status-publish has-post-thumbnail hentry portfolio-category-restaurant portfolio-tag-recipes portfolio-tag-sweet portfolio-tag-tasty">
                                                        <div class="eltdf-pl-item-inner">
                                                            <div class="eltdf-pli-image">
                                                                <img width="519" height="859"
                                                                     src="/images/Carousel-showcase-img-.jpg"
                                                                     class="attachment-full size-full wp-post-image"
                                                                     alt="a" loading="lazy"
                                                                     srcset="/images/Carousel-showcase-img-1.jpg 519w, /images/Carousel-showcase-img-1.jpg 181w"
                                                                     sizes="(max-width: 519px) 100vw, 519px"/>
                                                            </div>
                                                            <div class="eltdf-pli-text-holder">
                                                                <div class="eltdf-pli-text-wrapper">
                                                                    <div class="eltdf-pli-text">
                                                                        <h4 itemprop="name"
                                                                            class="eltdf-pli-title entry-title">
                                                                            {{ __('homepage.gallery-food-1') }}
                                                                        </h4>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </article>
                                                    <article
                                                            class="eltdf-pl-item eltdf-item-space  swiper-slide post-819 portfolio-item type-portfolio-item status-publish has-post-thumbnail hentry portfolio-category-restaurant portfolio-tag-cocktail portfolio-tag-tasty">
                                                        <div class="eltdf-pl-item-inner">
                                                            <div class="eltdf-pli-image">
                                                                <img width="567" height="859"
                                                                     src="/images/Carousel-showcase-img-2.jpg"
                                                                     class="attachment-full size-full wp-post-image"
                                                                     alt="a" loading="lazy"
                                                                     srcset="/images/Carousel-showcase-img-2.jpg 567w, /images/Carousel-showcase-img-2.jpg 198w"
                                                                     sizes="(max-width: 567px) 100vw, 567px"/>
                                                            </div>
                                                            <div class="eltdf-pli-text-holder">
                                                                <div class="eltdf-pli-text-wrapper">
                                                                    <div class="eltdf-pli-text">
                                                                        <h4 itemprop="name"
                                                                            class="eltdf-pli-title entry-title">
                                                                            {{ __('homepage.gallery-food-2') }}
                                                                             </h4>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </article>
                                                    <article
                                                            class="eltdf-pl-item eltdf-item-space  swiper-slide post-820 portfolio-item type-portfolio-item status-publish has-post-thumbnail hentry portfolio-category-restaurant portfolio-tag-recipes portfolio-tag-sweet portfolio-tag-tasty">
                                                        <div class="eltdf-pl-item-inner">
                                                            <div class="eltdf-pli-image">
                                                                <img width="794" height="859"
                                                                     src="/images/Carousel-showcase-img-3.jpg"
                                                                     class="attachment-full size-full wp-post-image"
                                                                     alt="a" loading="lazy"
                                                                     srcset="/images/Carousel-showcase-img-3.jpg 794w, /images/Carousel-showcase-img-3.jpg 277w, /images/Carousel-showcase-img-3.jpg 768w, /images/Carousel-showcase-img-3.jpg 600w"
                                                                     sizes="(max-width: 794px) 100vw, 794px"/>
                                                            </div>
                                                            <div class="eltdf-pli-text-holder">
                                                                <div class="eltdf-pli-text-wrapper">
                                                                    <div class="eltdf-pli-text">
                                                                        <h4 itemprop="name"
                                                                            class="eltdf-pli-title entry-title">
                                                                            {{ __('homepage.gallery-food-3') }}
                                                                        </h4>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </article>
                                                    <article
                                                            class="eltdf-pl-item eltdf-item-space  swiper-slide post-829 portfolio-item type-portfolio-item status-publish has-post-thumbnail hentry portfolio-category-restaurant portfolio-tag-recipes portfolio-tag-sweet portfolio-tag-tasty">
                                                        <div class="eltdf-pl-item-inner">
                                                            <div class="eltdf-pli-image">
                                                                <img width="513" height="859"
                                                                     src="/images/Carousel-showcase-img-4.jpg"
                                                                     class="attachment-full size-full wp-post-image"
                                                                     alt="a" loading="lazy"
                                                                     srcset="/images/Carousel-showcase-img-4.jpg 513w, /images/Carousel-showcase-img-4-179x300.jpg 179w"
                                                                     sizes="(max-width: 513px) 100vw, 513px"/>
                                                            </div>
                                                            <div class="eltdf-pli-text-holder">
                                                                <div class="eltdf-pli-text-wrapper">
                                                                    <div class="eltdf-pli-text">
                                                                        <h4 itemprop="name"
                                                                            class="eltdf-pli-title entry-title">
                                                                            {{ __('homepage.gallery-food-4') }}
                                                                             </h4>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </article>
                                                    <article
                                                            class="eltdf-pl-item eltdf-item-space  swiper-slide post-906 portfolio-item type-portfolio-item status-publish has-post-thumbnail hentry portfolio-category-restaurant portfolio-tag-recipes portfolio-tag-sweet portfolio-tag-tasty">
                                                        <div class="eltdf-pl-item-inner">
                                                            <div class="eltdf-pli-image">
                                                                <img width="639" height="859"
                                                                     src="/images/Carousel-showcase-img-5.jpg"
                                                                     class="attachment-full size-full wp-post-image"
                                                                     alt="a" loading="lazy"
                                                                     srcset="/images/Carousel-showcase-img-5.jpg 639w, /images/Carousel-showcase-img-5-223x300.jpg 223w, /images/Carousel-showcase-img-5-600x807.jpg 600w"
                                                                     sizes="(max-width: 639px) 100vw, 639px"/>
                                                            </div>
                                                            <div class="eltdf-pli-text-holder">
                                                                <div class="eltdf-pli-text-wrapper">
                                                                    <div class="eltdf-pli-text">
                                                                        <h4 itemprop="name"
                                                                            class="eltdf-pli-title entry-title">
                                                                            {{ __('homepage.gallery-food-5') }}
                                                                             </h4>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </article>
                                                    <article
                                                            class="eltdf-pl-item eltdf-item-space  swiper-slide post-1331 portfolio-item type-portfolio-item status-publish has-post-thumbnail hentry portfolio-category-restaurant portfolio-tag-recipes portfolio-tag-sweet portfolio-tag-tasty">
                                                        <div class="eltdf-pl-item-inner">
                                                            <div class="eltdf-pli-image">
                                                                <img width="728" height="859"
                                                                     src="/images/Carousel-showcase-img-6.jpg"
                                                                     class="attachment-full size-full wp-post-image"
                                                                     alt="a" loading="lazy"
                                                                     srcset="/images/Carousel-showcase-img-6.jpg 728w, /images/Carousel-showcase-img-6-254x300.jpg 254w, /images/Carousel-showcase-img-6-600x708.jpg 600w"
                                                                     sizes="(max-width: 728px) 100vw, 728px"/>
                                                            </div>
                                                            <div class="eltdf-pli-text-holder">
                                                                <div class="eltdf-pli-text-wrapper">
                                                                    <div class="eltdf-pli-text">
                                                                        <h4 itemprop="name"
                                                                            class="eltdf-pli-title entry-title">
                                                                            {{ __('homepage.gallery-food-6') }}
                                                                             </h4>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </article>
                                                    <article
                                                            class="eltdf-pl-item eltdf-item-space  swiper-slide post-1332 portfolio-item type-portfolio-item status-publish has-post-thumbnail hentry portfolio-category-restaurant portfolio-tag-recipes portfolio-tag-sweet portfolio-tag-tasty">
                                                        <div class="eltdf-pl-item-inner">
                                                            <div class="eltdf-pli-image">
                                                                <img width="555" height="859"
                                                                     src="/images/Carousel-showcase-img-7.jpg"
                                                                     class="attachment-full size-full wp-post-image"
                                                                     alt="a" loading="lazy"
                                                                     srcset="/images/Carousel-showcase-img-7.jpg 555w, /images/Carousel-showcase-img-7-194x300.jpg 194w"
                                                                     sizes="(max-width: 555px) 100vw, 555px"/>
                                                            </div>
                                                            <div class="eltdf-pli-text-holder">
                                                                <div class="eltdf-pli-text-wrapper">
                                                                    <div class="eltdf-pli-text">
                                                                        <h4 itemprop="name"
                                                                            class="eltdf-pli-title entry-title">
                                                                            {{ __('homepage.gallery-food-7') }}
                                                                             </h4>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </article>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

      <div class="eltdf-wrapper-inner">
            <footer class="eltdf-page-footer ">
                <div class="eltdf-footer-bottom-holder">
                </div>
            </footer>
        </div>

</div>


<div class="animate__animated animate__rotateIn menu-tester" ondblclick="off()" id="overlay"
     title="{{ __('homepage.menu-close-me') }}">
    <div class="eltdf-full-width">
        <div class="eltdf-full-width-inner">
            <div class="eltdf-grid-lines-holder eltdf-grid-columns-5">
                <div class="eltdf-grid-line eltdf-grid-column-1"></div>
                <div class="eltdf-grid-line eltdf-grid-column-2"></div>
                <div class="eltdf-grid-line eltdf-grid-column-3"></div>
                <div class="eltdf-grid-line eltdf-grid-column-4"></div>
                <div class="eltdf-grid-line eltdf-grid-column-5"></div>
            </div>
            <div class="eltdf-grid-row">

                <div class="eltdf-page-content-holder eltdf-grid-col-12">
                    <span class="close-menu" onclick="off()">X</span>
                    <img width="120" alt="logo"
                         src="/images/logo-pure.png"
                         class="menu-page-logo"
                    />

                    <div class="menu-cover">
                        <img src="/images/Our-menu-img-1.jpg" alt="menu-img" class="menu-img">
                    </div>
                    <br>
                    <div class="eltdf-row-grid-section-wrapper ">
                        <div class="eltdf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1573651731346">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">

                                            <div class="eltdf-section-title-holder    eltdf-st-decor-animation"
                                                 style="text-align: center;">
                                                <div class="eltdf-st-inner">
                                                    <br>

                                                    <span class="eltdf-st-tagline"
                                                          id="appetizer">
                                                    </span>

                                                    <div class="eltdf-st-title-holder">
                                                        <div class="decor">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="41.125" height="9.146">
                                                                <path fill="none" stroke="#9C7C57"
                                                                      stroke-miterlimit="10"
                                                                      d="M40.881 8.576L20.562.591.244 8.576"/>
                                                                <path fill="none" stroke="#9C7C57"
                                                                      stroke-miterlimit="10"
                                                                      d="M40.881.591L20.562 8.576.243.591"/>
                                                            </svg>
                                                        </div>
                                                        <h4 class="eltdf-st-title"> {{ __('homepage.menu-salad') }} </h4>
                                                        <div class="decor">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="41.125" height="9.146">
                                                                <path fill="none" stroke="#9C7C57"
                                                                      stroke-miterlimit="10"
                                                                      d="M40.881 8.576L20.562.591.244 8.576"/>
                                                                <path fill="none" stroke="#9C7C57"
                                                                      stroke-miterlimit="10"
                                                                      d="M40.881.591L20.562 8.576.243.591"/>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 63px"><span
                                                    class="vc_empty_space_inner"></span></div>
                                            <div
                                                class="eltdf-elements-holder   eltdf-two-columns  eltdf-responsive-mode-1024 col-r  fifty-fifty">
                                                <div class="eltdf-eh-item  "
                                                     data-item-class="eltdf-eh-custom-9724"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner ">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-9724"
                                                             style="">
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-starter-s-1') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-starter-s-1-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-starter-s-d-1') }}

                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-starter-s-2') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-starter-s-2-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-starter-s-d-2') }}

                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
{{--                                                                            <div--}}
{{--                                                                                style="width:60%; position:relative; right:15%">--}}
{{--                                                                                <img src="/images/download.png">--}}
{{--                                                                            </div>--}}
                                                                            {{ __('homepage.menu-starter-s-3') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-starter-s-3-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-starter-s-d-3') }}

                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-starter-s-4') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-starter-s-4-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-starter-s-d-4') }}
                                                                    </p>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="eltdf-eh-item"
                                                     data-item-class="eltdf-eh-custom-2722"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-2722"
                                                             style="padding: 0 0 0 42px">
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-starter-s-5') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-starter-s-5-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-starter-s-d-5') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-starter-s-6') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-starter-s-6-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-starter-s-d-6') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-starter-s-7') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-starter-s-7-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-starter-s-d-7') }}
                                                                    </p>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <h4 class="eltdf-st-title centered"> {{ __('homepage.menu-appetizer') }}  </h4>
                                            <div
                                                class="eltdf-elements-holder   eltdf-two-columns  eltdf-responsive-mode-1024  col-r fifty-fifty">
                                                <div class="eltdf-eh-item "
                                                     data-item-class="eltdf-eh-custom-9724"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-9724"
                                                             style="">
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-starter-a-1') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-starter-a-1-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-starter-a-d-1') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-starter-a-2') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-starter-a-2-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-starter-a-d-2') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-starter-a-3') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-starter-a-3-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-starter-a-d-3') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-starter-a-4') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-starter-a-4-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-starter-a-d-4') }}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="eltdf-eh-item    "
                                                     data-item-class="eltdf-eh-custom-2722"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-2722"
                                                             style="padding: 0 0 0 42px">
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-starter-a-5') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-starter-a-5-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-starter-a-d-5') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-starter-a-6') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-starter-a-6-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-starter-a-d-6') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-starter-a-7') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-starter-a-7-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-starter-a-d-7') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-starter-a-8') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-starter-a-8-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-starter-a-d-8') }}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="menu-cover">
                        <img src="/images/Our-menu-img-2.jpg" alt="menu-img" class="menu-img">
                    </div>
                    <br>
                    <div class="eltdf-row-grid-section-wrapper ">
                        <div class="eltdf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1573651794430">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="eltdf-section-title-holder    eltdf-st-decor-animation"
                                                 style="text-align: center">
                                                <br>
                                                <div class="eltdf-st-inner">

                                                    <span class="eltdf-st-tagline">    </span>
                                                    <div class="eltdf-st-title-holder">
                                                        <div class="decor">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="41.125" height="9.146">
                                                                <path fill="none" stroke="#9C7C57"
                                                                      stroke-miterlimit="10"
                                                                      d="M40.881 8.576L20.562.591.244 8.576"/>
                                                                <path fill="none" stroke="#9C7C57"
                                                                      stroke-miterlimit="10"
                                                                      d="M40.881.591L20.562 8.576.243.591"/>
                                                            </svg>
                                                        </div>
                                                        <h4 class="eltdf-st-title"
                                                            id="entre">{{ __('homepage.menu-main') }} </h4>
                                                        <div class="decor">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="41.125" height="9.146">
                                                                <path fill="none" stroke="#9C7C57"
                                                                      stroke-miterlimit="10"
                                                                      d="M40.881 8.576L20.562.591.244 8.576"/>
                                                                <path fill="none" stroke="#9C7C57"
                                                                      stroke-miterlimit="10"
                                                                      d="M40.881.591L20.562 8.576.243.591"/>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 63px"><span
                                                    class="vc_empty_space_inner"></span></div>

                                            <h4 class="eltdf-st-title centered"> {{ __('homepage.menu-italy') }}  </h4>

                                            <div
                                                class="eltdf-elements-holder   eltdf-two-columns  eltdf-responsive-mode-1024 col-r fifty-fifty">
                                                <div class="eltdf-eh-item    "
                                                     data-item-class="eltdf-eh-custom-8229"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-8229"
                                                             style="">
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-p-1') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-p-1-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-p-d-1') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-p-2') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-p-2-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-p-d-2') }}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="eltdf-eh-item    "
                                                     data-item-class="eltdf-eh-custom-3108"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-3108"
                                                             style="padding: 0 0 0 42px">
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-p-3') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-p-3-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-p-d-3') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-p-5') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-p-5-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-p-d-5') }}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <h4 class="eltdf-st-title centered"> {{ __('homepage.menu-burger') }}  </h4>

                                            <div
                                                class="eltdf-elements-holder   eltdf-two-columns  eltdf-responsive-mode-1024  col-r fifty-fifty">
                                                <div class="eltdf-eh-item    "
                                                     data-item-class="eltdf-eh-custom-8229"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-8229"
                                                             style="">
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-b-1') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-b-1-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-b-d-1') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-b-2') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-b-2-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-b-d-2') }}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="eltdf-eh-item    "
                                                     data-item-class="eltdf-eh-custom-3108"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-3108"
                                                             style="padding: 0 0 0 42px">
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-b-3') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-b-3-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-b-d-3') }}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <h4 class="eltdf-st-title centered"> {{ __('homepage.menu-italy-2') }}  </h4>

                                            <div
                                                class="eltdf-elements-holder   eltdf-two-columns  eltdf-responsive-mode-1024 col-r fifty-fifty">
                                                <div class="eltdf-eh-item    "
                                                     data-item-class="eltdf-eh-custom-8229"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-8229"
                                                             style="">
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-p-6') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-p-6-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-p-d-6') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-p-7') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-p-7-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-p-d-7') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-p-8') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-p-8-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-p-d-8') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-p-9') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-p-9-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-p-d-9') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-p-10') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-p-10-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-p-d-10') }}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="eltdf-eh-item    "
                                                     data-item-class="eltdf-eh-custom-3108"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-3108"
                                                             style="padding: 0 0 0 42px">
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-p-11') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-p-11-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-p-d-11') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-p-12') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-p-12-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-p-d-12') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-p-13') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-p-13-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-p-d-13') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-p-14') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-p-14-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-p-d-14') }}
                                                                    </p>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <h4 class="eltdf-st-title centered"> {{ __('homepage.menu-kebob') }}  </h4>

                                            <div
                                                class="eltdf-elements-holder   eltdf-two-columns  eltdf-responsive-mode-1024  col-r fifty-fifty">
                                                <div class="eltdf-eh-item"
                                                     data-item-class="eltdf-eh-custom-8229"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-8229"
                                                        >
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-c-1') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price"> {{ __('homepage.menu-main-c-1-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-c-d-1') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-c-2') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-c-2-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-c-d-2') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-c-3') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-c-3-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-c-d-3') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-c-4') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-c-4-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-c-d-4') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-c-5') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-c-5-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-c-d-5') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-c-6') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-c-6-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-c-d-6') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-c-10') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-c-10-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-c-d-10') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-c-11') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-c-11-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-c-d-11') }}
                                                                    </p>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="eltdf-eh-item"
                                                     data-item-class="eltdf-eh-custom-3108"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-3108"
                                                             style="padding: 0 0 0 42px">
                                                            <div class="eltdf-pricing-holder ">

                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-c-12') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-c-12-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-c-d-12') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-c-13') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-c-13-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-c-d-13') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-c-14') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-c-14-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-c-d-14') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-c-15') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-c-15-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-c-d-15') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-c-16') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-c-16-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-c-d-16') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-c-17') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-c-17-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-c-d-17') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-c-18') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-c-18-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-c-d-18') }}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <h4 class="eltdf-st-title centered"> {{ __('homepage.menu-sea') }}  </h4>

                                            <div
                                                class="eltdf-elements-holder   eltdf-two-columns  eltdf-responsive-mode-1024  col-r fifty-fifty">
                                                <div class="eltdf-eh-item"
                                                     data-item-class="eltdf-eh-custom-8229"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-8229"
                                                        >
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-p-4') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-p-4-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-p-d-4') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-c-7') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-c-7-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-c-d-7') }}
                                                                    </p>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="eltdf-eh-item"
                                                     data-item-class="eltdf-eh-custom-3108"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-3108"
                                                             style="padding: 0 0 0 42px">
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-c-8') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-c-8-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-c-d-8') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-main-c-9') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-main-c-9-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-main-c-d-9') }}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="menu-cover">
                        <img src="/images/bar-home-rev-3.jpg" alt="menu-img" class="menu-img">
                    </div>
                    <br>
                    <div class="eltdf-row-grid-section-wrapper ">
                        <div class="eltdf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1573651846472">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="eltdf-section-title-holder    eltdf-st-decor-animation"
                                                 style="text-align: center">
                                                <div class="eltdf-st-inner">
                                                    <span class="whitish eltdf-st-tagline">
                                                        {{ __('homepage.menu-drink') }}
                                                    </span>
                                                    <div class="eltdf-st-title-holder">
                                                        <div class="decor">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="41.125" height="9.146">
                                                                <path fill="none" stroke="#9C7C57"
                                                                      stroke-miterlimit="10"
                                                                      d="M40.881 8.576L20.562.591.244 8.576"/>
                                                                <path fill="none" stroke="#9C7C57"
                                                                      stroke-miterlimit="10"
                                                                      d="M40.881.591L20.562 8.576.243.591"/>
                                                            </svg>
                                                        </div>
                                                        <h4 class="eltdf-st-title whitish" id="dessert">
                                                            {{ __('homepage.menu-hot') }}
                                                        </h4>
                                                        <div class="decor">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="41.125" height="9.146">
                                                                <path fill="none" stroke="#9C7C57"
                                                                      stroke-miterlimit="10"
                                                                      d="M40.881 8.576L20.562.591.244 8.576"/>
                                                                <path fill="none" stroke="#9C7C57"
                                                                      stroke-miterlimit="10"
                                                                      d="M40.881.591L20.562 8.576.243.591"/>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 63px"><span
                                                    class="vc_empty_space_inner"></span></div>
                                            <div
                                                class="eltdf-elements-holder   eltdf-two-columns  eltdf-responsive-mode-1024 col-r  fifty-fifty">
                                                <div class="eltdf-eh-item    "
                                                     data-item-class="eltdf-eh-custom-7307"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-7307"
                                                             style="">
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-h-1') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">  {{ __('homepage.menu-drink-h-1-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-h-2') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">  {{ __('homepage.menu-drink-h-2-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-h-3') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">  {{ __('homepage.menu-drink-h-3-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-h-4') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">  {{ __('homepage.menu-drink-h-4-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-h-5') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">  {{ __('homepage.menu-drink-h-5-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-h-6') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">  {{ __('homepage.menu-drink-h-6-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-h-7') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">  {{ __('homepage.menu-drink-h-7-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-h-8') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">  {{ __('homepage.menu-drink-h-8-price') }}</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="eltdf-eh-item    "
                                                     data-item-class="eltdf-eh-custom-7039"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-7039"
                                                             style="padding: 0 0 0 42px">
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-h-9') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">  {{ __('homepage.menu-drink-h-9-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-h-10') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">  {{ __('homepage.menu-drink-h-10-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-h-11') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">  {{ __('homepage.menu-drink-h-11-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-h-12') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">  {{ __('homepage.menu-drink-h-12-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-h-13') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">  {{ __('homepage.menu-drink-h-13-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-h-14') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">  {{ __('homepage.menu-drink-h-14-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-h-15') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">  {{ __('homepage.menu-drink-h-15-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-h-16') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">  {{ __('homepage.menu-drink-h-16-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-h-17') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">  {{ __('homepage.menu-drink-h-17-price') }}</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <h4 class="eltdf-st-title centered whitish">  {{ __('homepage.menu-cold') }} </h4>

                                            <div
                                                class="eltdf-elements-holder   eltdf-two-columns  eltdf-responsive-mode-1024 col-r fifty-fifty">
                                                <div class="eltdf-eh-item    "
                                                     data-item-class="eltdf-eh-custom-7307"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-7307"
                                                             style="">
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-c-1') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-c-1-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-c-2') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-c-2-price') }}</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="eltdf-eh-item    "
                                                     data-item-class="eltdf-eh-custom-7039"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-7039"
                                                             style="padding: 0 0 0 42px">
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-c-3') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-c-3-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-c-4') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-c-4-price') }}</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <h4 class="eltdf-st-title centered whitish">  {{ __('homepage.menu-ht') }} </h4>

                                            <div
                                                class="eltdf-elements-holder   eltdf-two-columns  eltdf-responsive-mode-1024 col-r fifty-fifty">
                                                <div class="eltdf-eh-item    "
                                                     data-item-class="eltdf-eh-custom-7307"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-7307"
                                                             style="">
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-ht-1') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-ht-1-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-ht-2') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-ht-2-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-ht-3') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-ht-3-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-ht-4') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-ht-4-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-ht-5') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-ht-5-price') }}</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="eltdf-eh-item"
                                                     data-item-class="eltdf-eh-custom-7039"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-7039"
                                                             style="padding: 0 0 0 42px">
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-ht-6') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-ht-6-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-ht-7') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-ht-7-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-ht-8') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-ht-8-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-ht-9') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-ht-9-price') }}</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <h4 class="eltdf-st-title centered whitish"> {{ __('homepage.menu-ms') }} </h4>

                                            <div
                                                class="eltdf-elements-holder   eltdf-two-columns  eltdf-responsive-mode-1024 col-r fifty-fifty">
                                                <div class="eltdf-eh-item    "
                                                     data-item-class="eltdf-eh-custom-7307"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-7307"
                                                             style="">
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-ms-1') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price"> {{ __('homepage.menu-drink-ms-1-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-ms-2') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-ms-2-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-ms-3') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-ms-3-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-ms-4') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-ms-4-price') }}</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="eltdf-eh-item    "
                                                     data-item-class="eltdf-eh-custom-7039"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-7039"
                                                             style="padding: 0 0 0 42px">
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-ms-5') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-ms-5-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-ms-6') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-ms-6-price') }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-ms-7') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-ms-7-price') }}</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>





                                            {{--                                            <h4 class="eltdf-st-title centered whitish">  {{ __('homepage.menu-fj') }} </h4>--}}

                                            {{--                                            <div class="eltdf-elements-holder   eltdf-two-columns  eltdf-responsive-mode-1024  fifty-fifty">--}}
                                            {{--                                                <div class="eltdf-eh-item    "--}}
                                            {{--                                                     data-item-class="eltdf-eh-custom-7307"--}}
                                            {{--                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">--}}
                                            {{--                                                    <div class="eltdf-eh-item-inner">--}}
                                            {{--                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-7307"--}}
                                            {{--                                                             style="padding: 0 42px 0 0">--}}
                                            {{--                                                            <div class="eltdf-pricing-holder ">--}}
                                            {{--                                                                <div class="eltdf-pricing-item">--}}
                                            {{--                                                                    <div class="eltdf-pricing-main">--}}
                                            {{--                                                                        <h6 class="eltdf-pricing-title">--}}
                                            {{--                                                                            {{ __('homepage.menu-drink-f-1') }}--}}
                                            {{--                                                                        </h6>--}}
                                            {{--                                                                        <div class="eltdf-pricing-lines"></div><span class="eltdf-pricing-price">35</span>--}}
                                            {{--                                                                    </div>--}}
                                            {{--                                                                </div>--}}
                                            {{--                                                            </div>--}}
                                            {{--                                                        </div>--}}
                                            {{--                                                    </div>--}}
                                            {{--                                                </div>--}}
                                            {{--                                            </div>--}}



                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="menu-cover">
                        <img src="/images/Our-menu-img-3.jpg" alt="menu-img" class="menu-img">
                    </div>
                    <br>
                    <div class="eltdf-row-grid-section-wrapper ">
                        <div class="eltdf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1573651731346">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="eltdf-section-title-holder    eltdf-st-decor-animation"
                                                 style="text-align: center;">
                                                <div class="eltdf-st-inner">

                                                    <div class="eltdf-st-title-holder">
                                                        <div class="decor">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="41.125" height="9.146">
                                                                <path fill="none" stroke="#9C7C57"
                                                                      stroke-miterlimit="10"
                                                                      d="M40.881 8.576L20.562.591.244 8.576"/>
                                                                <path fill="none" stroke="#9C7C57"
                                                                      stroke-miterlimit="10"
                                                                      d="M40.881.591L20.562 8.576.243.591"/>
                                                            </svg>
                                                        </div>

                                                        <h4 class="eltdf-st-title"> {{ __('homepage.menu-d') }} </h4>

                                                        <div class="decor">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="41.125" height="9.146">
                                                                <path fill="none" stroke="#9C7C57"
                                                                      stroke-miterlimit="10"
                                                                      d="M40.881 8.576L20.562.591.244 8.576"/>
                                                                <path fill="none" stroke="#9C7C57"
                                                                      stroke-miterlimit="10"
                                                                      d="M40.881.591L20.562 8.576.243.591"/>
                                                            </svg>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                            <div class="vc_empty_space" style="height: 63px"><span
                                                    class="vc_empty_space_inner"></span></div>
                                            <div
                                                class="eltdf-elements-holder   eltdf-two-columns  eltdf-responsive-mode-1024 col-r fifty-fifty">
                                                <div class="eltdf-eh-item    "
                                                     data-item-class="eltdf-eh-custom-7307"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-7307"
                                                             style="padding: 0 42px 0 0">
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-dessert-1') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price"> {{ __('homepage.menu-dessert-1-price') }}</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="eltdf-eh-item    "
                                                     data-item-class="eltdf-eh-custom-7039"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-7039"
                                                             style="padding: 0 0 0 42px">
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-dessert-2') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price"> {{ __('homepage.menu-dessert-2-price') }}</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-dessert-3') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price"> {{ __('homepage.menu-dessert-3-price') }}</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <h4 class="eltdf-st-title centered whitish">  {{ __('homepage.menu-m') }} </h4>

                                            <div
                                                class="eltdf-elements-holder   eltdf-two-columns  eltdf-responsive-mode-1024  col-r fifty-fifty">
                                                <div class="eltdf-eh-item    "
                                                     data-item-class="eltdf-eh-custom-7307"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-7307"
                                                             style="">
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-m-1') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price"> {{ __('homepage.menu-drink-m-1-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-drink-m-d-1') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-m-2') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-m-2-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-drink-m-d-2') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-m-3') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-m-3-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-drink-m-d-3') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-m-4') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-m-4-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-drink-m-d-4') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-m-5') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-m-5-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-drink-m-d-5') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-m-6') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-m-6-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-drink-m-d-6') }}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="eltdf-eh-item    "
                                                     data-item-class="eltdf-eh-custom-7039"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-7039"
                                                             style="padding: 0 0 0 42px">
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-m-7') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-m-7-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-drink-m-d-7') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-m-8') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-m-8-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-drink-m-d-8') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-m-9') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-m-9-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-drink-m-d-9') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-m-10') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-m-10-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-drink-m-d-10') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-m-11') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-m-11-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-drink-m-d-11') }}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <h4 class="eltdf-st-title centered whitish">  {{ __('homepage.menu-sm') }}  </h4>

                                            <div
                                                class="eltdf-elements-holder   eltdf-two-columns  eltdf-responsive-mode-1024 col-r fifty-fifty">
                                                <div class="eltdf-eh-item    "
                                                     data-item-class="eltdf-eh-custom-7307"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-7307"
                                                             style="">
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-s-1') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-s-1-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-drink-s-d-1') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-s-2') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-s-2-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-drink-s-d-2') }}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="eltdf-eh-item    "
                                                     data-item-class="eltdf-eh-custom-7039"
                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">
                                                    <div class="eltdf-eh-item-inner">
                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-7039"
                                                             style="padding: 0 0 0 42px">
                                                            <div class="eltdf-pricing-holder ">
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-s-3') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-s-3-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-drink-s-d-3') }}
                                                                    </p>
                                                                </div>
                                                                <div class="eltdf-pricing-item">
                                                                    <div class="eltdf-pricing-main">
                                                                        <h6 class="eltdf-pricing-title">
                                                                            {{ __('homepage.menu-drink-s-4') }}
                                                                        </h6>
                                                                        <div class="eltdf-pricing-lines"></div>
                                                                        <span
                                                                            class="eltdf-pricing-price">{{ __('homepage.menu-drink-s-4-price') }}</span>
                                                                    </div>
                                                                    <p class="eltdf-pricing-desc">
                                                                        {{ __('homepage.menu-drink-s-d-4') }}
                                                                    </p>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {{--                                            <h4 class="eltdf-st-title centered">  {{ __('homepage.menu-br') }} </h4>--}}
                                            {{--                                            <div class="eltdf-elements-holder   eltdf-two-columns  eltdf-responsive-mode-1024  fifty-fifty">--}}
                                            {{--                                                <div class="eltdf-eh-item    "--}}
                                            {{--                                                     data-item-class="eltdf-eh-custom-7307"--}}
                                            {{--                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">--}}
                                            {{--                                                    <div class="eltdf-eh-item-inner">--}}
                                            {{--                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-7307"--}}
                                            {{--                                                             style="padding: 0 42px 0 0">--}}
                                            {{--                                                            <div class="eltdf-pricing-holder ">--}}
                                            {{--                                                                <div class="eltdf-pricing-item">--}}
                                            {{--                                                                    <div class="eltdf-pricing-main">--}}
                                            {{--                                                                        <h6 class="eltdf-pricing-title">--}}
                                            {{--                                                                            {{ __('homepage.menu-breakfast-1') }}--}}
                                            {{--                                                                        </h6>--}}
                                            {{--                                                                        <div class="eltdf-pricing-lines"></div><span class="eltdf-pricing-price">59</span>--}}
                                            {{--                                                                    </div>--}}
                                            {{--                                                                    <p class="eltdf-pricing-desc">--}}
                                            {{--                                                                        {{ __('homepage.menu-breakfast-d-1') }}--}}
                                            {{--                                                                    </p>--}}
                                            {{--                                                                </div>--}}
                                            {{--                                                                <div class="eltdf-pricing-item">--}}
                                            {{--                                                                    <div class="eltdf-pricing-main">--}}
                                            {{--                                                                        <h6 class="eltdf-pricing-title">--}}
                                            {{--                                                                            {{ __('homepage.menu-breakfast-2') }}--}}
                                            {{--                                                                        </h6>--}}
                                            {{--                                                                        <div class="eltdf-pricing-lines"></div><span class="eltdf-pricing-price">98</span>--}}
                                            {{--                                                                    </div>--}}
                                            {{--                                                                    <p class="eltdf-pricing-desc">--}}
                                            {{--                                                                        {{ __('homepage.menu-breakfast-d-2') }}--}}
                                            {{--                                                                    </p>--}}
                                            {{--                                                                </div>--}}
                                            {{--                                                                <div class="eltdf-pricing-item">--}}
                                            {{--                                                                    <div class="eltdf-pricing-main">--}}
                                            {{--                                                                        <h6 class="eltdf-pricing-title">--}}
                                            {{--                                                                            {{ __('homepage.menu-breakfast-3') }}--}}
                                            {{--                                                                        </h6>--}}
                                            {{--                                                                        <div class="eltdf-pricing-lines"></div><span class="eltdf-pricing-price">53</span>--}}
                                            {{--                                                                    </div>--}}
                                            {{--                                                                    <p class="eltdf-pricing-desc">--}}
                                            {{--                                                                        {{ __('homepage.menu-breakfast-d-3') }}--}}
                                            {{--                                                                    </p>--}}
                                            {{--                                                                </div>--}}
                                            {{--                                                            </div>--}}
                                            {{--                                                        </div>--}}
                                            {{--                                                    </div>--}}
                                            {{--                                                </div>--}}
                                            {{--                                                <div class="eltdf-eh-item    "--}}
                                            {{--                                                     data-item-class="eltdf-eh-custom-7039"--}}
                                            {{--                                                     data-769-1024="0 0" data-681-768="0 0" data-680="0 0">--}}
                                            {{--                                                    <div class="eltdf-eh-item-inner">--}}
                                            {{--                                                        <div class="eltdf-eh-item-content eltdf-eh-custom-7039"--}}
                                            {{--                                                             style="padding: 0 0 0 42px">--}}
                                            {{--                                                            <div class="eltdf-pricing-holder ">--}}
                                            {{--                                                                <div class="eltdf-pricing-item">--}}
                                            {{--                                                                    <div class="eltdf-pricing-main">--}}
                                            {{--                                                                        <h6 class="eltdf-pricing-title">--}}
                                            {{--                                                                            {{ __('homepage.menu-breakfast-4') }}--}}
                                            {{--                                                                        </h6>--}}
                                            {{--                                                                        <div class="eltdf-pricing-lines"></div><span class="eltdf-pricing-price">56</span>--}}
                                            {{--                                                                    </div>--}}
                                            {{--                                                                    <p class="eltdf-pricing-desc">--}}
                                            {{--                                                                        {{ __('homepage.menu-breakfast-d-4') }}--}}
                                            {{--                                                                    </p>--}}
                                            {{--                                                                </div>--}}
                                            {{--                                                                <div class="eltdf-pricing-item">--}}
                                            {{--                                                                    <div class="eltdf-pricing-main">--}}
                                            {{--                                                                        <h6 class="eltdf-pricing-title">--}}
                                            {{--                                                                            {{ __('homepage.menu-breakfast-5') }}--}}
                                            {{--                                                                        </h6>--}}
                                            {{--                                                                        <div class="eltdf-pricing-lines"></div><span class="eltdf-pricing-price">46</span>--}}
                                            {{--                                                                    </div>--}}
                                            {{--                                                                    <p class="eltdf-pricing-desc">--}}
                                            {{--                                                                        {{ __('homepage.menu-breakfast-d-5') }}--}}
                                            {{--                                                                    </p>--}}
                                            {{--                                                                </div>--}}
                                            {{--                                                                <div class="eltdf-pricing-item">--}}
                                            {{--                                                                    <div class="eltdf-pricing-main">--}}
                                            {{--                                                                        <h6 class="eltdf-pricing-title">--}}
                                            {{--                                                                            {{ __('homepage.menu-breakfast-6') }}--}}
                                            {{--                                                                        </h6>--}}
                                            {{--                                                                        <div class="eltdf-pricing-lines"></div><span class="eltdf-pricing-price">59</span>--}}
                                            {{--                                                                    </div>--}}
                                            {{--                                                                    <p class="eltdf-pricing-desc">--}}
                                            {{--                                                                        {{ __('homepage.menu-breakfast-d-6') }}--}}
                                            {{--                                                                    </p>--}}
                                            {{--                                                                </div>--}}
                                            {{--                                                            </div>--}}
                                            {{--                                                        </div>--}}
                                            {{--                                                    </div>--}}
                                            {{--                                                </div>--}}
                                            {{--                                            </div>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <footer class="eltdf-page-footer">
        <div class="eltdf-footer-top-holder">
            <div class="eltdf-grid-row eltdf-footer-top-alignment-center">
                <div id="media_image-2" class="widget eltdf-footer-column-1 widget_media_image">
                    <img width="46" height="66" src="/images/footer-logo.png"
                         class="image wp-image-75  attachment-full size-full"
                         alt="u" loading="lazy"
                         style="max-width: 100%; height: auto; padding-bottom: 10px"/>
                </div>
            </div>
        </div>
    </footer>
</div>

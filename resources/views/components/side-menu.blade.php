<section class="eltdf-side-menu" id="side-menu">
    <a class="eltdf-close-side-menu eltdf-close-side-menu-predefined" href="#">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18.1 18.1" class="eltdf-menu-closer">
            <line x1="0.4" y1="0.4" x2="17.7" y2="17.7" class="a"/>
            <line x1="0.4" y1="17.7" x2="17.7" y2="0.4"/>
        </svg>
    </a>
    <div id="text-6" class="widget eltdf-sidearea widget_text">
        <div class="textwidget">
            <a href="index.html">
                <img width="200"
                     src="/images/logo-pure.png"
                     class="image wp-image-75  attachment-full size-full"
                     alt="logo"
                />
            </a>
            <br>
            <a href="https://maps.google.com/maps?width=520&amp;height=400&amp;hl=en&amp;q=Tehran%20Province,%20Tehran,%20Mehr%20Mohammadi%20Rd,%20Iran%20Tehran+(Garnet%20Luxury%20Cafe%20)&amp;t=&amp;z=15&amp;ie=UTF8&amp;iwloc=B&amp"
               target="_blank" rel="noopener noreferrer" class="just-me">
                {{ __('homepage.side-menu-address') }}
            </a>
            <br><br>
            <a href="tel:021-49927">
                {{ __('homepage.side-menu-tel') }}
            </a>
            <br><br>
            <p class="just-me">
                {{ __('homepage.side-menu-open') }}
            </p>

        </div>

    </div>
    <div class="widget eltdf-separator-widget">
        <div class="eltdf-separator-holder clearfix  eltdf-separator-center eltdf-separator-normal">
            <div class="eltdf-separator"
                 style="border-style: solid;border-bottom-width: 4px;margin-top: 0px;margin-bottom: 0px"></div>
        </div>
    </div>
    <div id="nav_menu-3" class="widget eltdf-sidearea widget_nav_menu">
        <div class="menu-footer-menu-container">
            <ul id="menu-footer-menu-1" class="menu">
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-70">
                    <a target="_blank" rel="noopener noreferrer" href="https://www.instagram.com/garnet_restaurant"
                       title="{{ __('homepage.IG') }}">
                        <i class="eltdf-icon-ion-icon ion-social-instagram-outline eltdf-author-social-instagram eltdf-author-social-icon "
                           style="font-size: 37px;color: #c9ab81;">
                        </i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <a href=" {{ route('admin') }}" title="{{ __('homepage.panel') }}">
        <img class="admin" alt="admin" src="/images/admin.png" width="58">
    </a>
</section>

<div id="about">
    <div class="eltdf-row-grid-section-wrapper eltdf-content-aligment-center" >
        <div class="eltdf-row-grid-section">
            <div class="vc_row wpb_row vc_row-fluid vc_custom_1578664464836 logo-garnet">
                <img  width="200"
                      src="/images/logo-pure.png"
                      class="image wp-image-75  attachment-full size-full logo-garnet"
                      alt="garnet-logo"
                      loading="lazy"/>
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="eltdf-elements-holder   eltdf-one-column  eltdf-responsive-mode-768  ">
                                <div class="eltdf-eh-item    "
                                     data-item-class="eltdf-eh-custom-3636"
                                     data-1400-1600="0 27% 40px 27%"
                                     data-1025-1399="0 23% 13px 23%"
                                     data-769-1024="0 20% 70px 20%"
                                     data-681-768="0 10% 0px 10%" data-680="0 2% 130px 2%">
                                    <div class="eltdf-eh-item-inner">
                                        <div class="eltdf-eh-item-content eltdf-eh-custom-3636"
                                             style="padding: 0 31% 70px 31%">
                                            <div class="eltdf-section-title-holder    eltdf-st-decor-animation"
                                                 style="text-align: center">
                                                <div class="eltdf-st-inner">
                                                    <span class="eltdf-st-tagline">  {{ __('homepage.about-us-head') }}   </span>
                                                    <div class="eltdf-st-title-holder">
                                                        <div class="decor">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="41.125"
                                                                 height="9.146">
                                                                <path fill="none"
                                                                      stroke="#9C7C57"
                                                                      stroke-miterlimit="10"
                                                                      d="M40.881 8.576L20.562.591.244 8.576"/>
                                                                <path fill="none"
                                                                      stroke-miterlimit="10"
                                                                      d="M40.881.591L20.562 8.576.243.591"/>
                                                            </svg>
                                                        </div>
                                                        <h1 class="eltdf-st-title"
                                                            id="story">
                                                            {{ __('homepage.about-us-title') }}

                                                        </h1>
                                                        <div class="decor">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="41.125"
                                                                 height="9.146">
                                                                <path fill="none"
                                                                      stroke="#9C7C57"
                                                                      stroke-miterlimit="10"
                                                                      d="M40.881 8.576L20.562.591.244 8.576"/>
                                                                <path fill="none"
                                                                      stroke="#9C7C57"
                                                                      stroke-miterlimit="10"
                                                                      d="M40.881.591L20.562 8.576.243.591"/>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <p class="eltdf-st-text">
                                                      {{ __('homepage.about-us-text') }}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="eltdf-row-grid-section-wrapper eltdf-content-aligment-center">
        <div class="eltdf-row-grid-section">
            <div class="vc_row wpb_row vc_row-fluid">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner vc_custom_1578912658047">
                        <div class="wpb_wrapper">
                            <div class="eltdf-elements-holder   eltdf-three-columns  eltdf-responsive-mode-680  ">
                                <div class="eltdf-eh-item    "
                                     data-item-class="eltdf-eh-custom-5532"
                                     data-1400-1600="170px 0 132px 0"
                                     data-1025-1399="150px 0 160px 0"
                                     data-769-1024="0 0 50px 0" data-681-768="0 0"
                                     data-680="0 0">
                                    <div class="eltdf-eh-item-inner">
                                        <div class="eltdf-eh-item-content eltdf-eh-custom-5532"
                                             style="padding: 195px 10px 160px 0">
                                            <div class="eltdf-single-image-holder     eltdf-image-appear-from-top">
                                                <div class="eltdf-si-inner">
                                                    <img width="800" height="1013"
                                                         src="/images/main-img-1.jpg"
                                                         class="attachment-full size-full"
                                                         alt="d" loading="lazy"
                                                         srcset="/images/main-img-1.jpg 800w, /images/main-img-1-600x760.jpg 600w, /images/main-img-1-237x300.jpg 237w, /images/main-img-1-768x972.jpg 768w"
                                                         sizes="(max-width: 800px) 100vw, 800px"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="eltdf-eh-item    "
                                     data-item-class="eltdf-eh-custom-8017"
                                     data-1400-1600="170px 0 132px 0"
                                     data-1025-1399="150px 0 160px 0"
                                     data-769-1024="0 0 50px 0" data-681-768="0 0"
                                     data-680="80px 0">
                                    <div class="eltdf-eh-item-inner">
                                        <div class="eltdf-eh-item-content eltdf-eh-custom-8017"
                                             style="padding: 202px 73px 160px 73px">
                                            <div class="eltdf-single-image-holder eltdf-main-home-middle-svg-pattern eltdf-svg-pattern   eltdf-image-appear-none">
                                                <div class="eltdf-si-inner">
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                         width="289.828" height="543.424">
                                                        <path fill="none" stroke="#9A7D57"
                                                              stroke-width="1.5"
                                                              stroke-miterlimit="10"
                                                              d="M0 0l72.457 72.457L0 144.913l72.457 72.457L0 289.826l72.457 72.456L0 434.739l72.457 72.456-36.229 36.229M72.457 0L0 72.457l72.457 72.457L0 217.37l72.457 72.457L0 362.282l72.457 72.457L0 507.195l36.229 36.229m72.457 0l36.229-36.229-72.457-72.456 72.457-72.457-72.457-72.456 72.457-72.457-72.457-72.457 72.457-72.457L72.457 0m72.457 0L72.457 72.457l72.457 72.457-72.457 72.456 72.457 72.457-72.457 72.456 72.457 72.457-72.457 72.456 36.229 36.229m72.457-.001l36.229-36.229-72.457-72.456 72.457-72.457-72.457-72.456 72.457-72.457-72.457-72.457 72.457-72.457L144.914 0m72.457 0l-72.457 72.457 72.457 72.457-72.457 72.457 72.457 72.457-72.457 72.456 72.457 72.457-72.457 72.456 36.229 36.229m72.457-.002l36.229-36.229-72.457-72.456 72.457-72.457-72.457-72.456 72.457-72.457-72.457-72.457 72.457-72.457L217.371 0m72.457 0l-72.457 72.457 72.457 72.457-72.457 72.457 72.457 72.457-72.457 72.456 72.457 72.457-72.457 72.456 36.229 36.229M18.113 54.343L0 36.229m54.342-18.115L36.229 0M18.113 18.114L36.228 0M126.8 18.114L108.686 0M90.571 18.114L108.686 0m90.571 18.114L181.143 0m0 0l-18.114 18.114m108.56 0L253.475 0M235.36 18.114L253.475 0m18.24 54.343l18.113-18.114m-271.715 90.57L0 108.685m271.715 18.114l18.113-18.114M18.113 199.256L0 181.142m271.715 18.114l18.113-18.114m-271.715 90.57L0 253.598m271.715 18.114l18.113-18.114M72.457 0v36.229L90.57 54.343m-36.029 0l17.916-18.114M144.914 0v36.229l18.113 18.114m-36.029 0l17.916-18.114M217.368 0v36.229l18.114 18.114M36.229 36.229v36.229l18.113 18.114m54.344-54.343v36.229L126.8 90.571m54.343-54.342v36.229l18.114 18.114M253.6 36.229v36.229l18.113 18.114m-72.262-36.229l17.917-18.114M72.457 72.457v36.229L90.57 126.8m-36.029-.001l17.916-18.114m72.457-36.228v36.229l18.113 18.114m-36.029-.001l17.916-18.114m72.447-36.228v36.229l18.114 18.114m-36.03-.001l17.916-18.114M72.457 144.913v36.229l18.113 18.114m-36.029 0l17.916-18.114m72.457-36.229v36.229l18.113 18.114m-36.029 0l17.916-18.114m72.447-36.229v36.229l18.114 18.114m-36.03 0l17.916-18.114M72.457 217.37v36.229l18.113 18.114m-36.029-.001l17.916-18.114m72.457-36.228v36.229l18.113 18.114m-36.029-.001l17.916-18.114m72.447-36.228v36.229l18.114 18.114m-36.03-.001l17.916-18.114M72.457 289.827v36.229L90.57 344.17m-36.029 0l17.916-18.114m72.457-36.229v36.229l18.113 18.114m-36.029 0l17.916-18.114m72.447-36.229v36.229l18.114 18.114m-36.03 0l17.916-18.114M72.457 362.282v36.229l18.113 18.114m-36.029 0l17.916-18.114m72.457-36.229v36.229l18.113 18.114m-36.029 0l17.916-18.114m72.447-36.229v36.229l18.114 18.114m-36.03 0l17.916-18.114M72.457 434.739v36.229l18.113 18.114m-36.029 0l17.916-18.114m72.457-36.229v36.229l18.113 18.114m-36.029 0l17.916-18.114m72.447-36.229v36.229l18.114 18.114m-36.03 0l17.916-18.114M18.313 90.571l17.916-18.114M90.77 90.571l17.916-18.114m54.541 18.114l17.916-18.114m54.541 18.114L253.6 72.457M36.229 108.685v36.229l18.113 18.114m54.344-54.343v36.229l18.114 18.114m54.343-54.343v36.229l18.114 18.114m54.343-54.343v36.229l18.113 18.114m-253.4 0l17.916-18.114m54.541 18.114l17.916-18.114m72.457 0l-17.916 18.114m72.457 0l17.916-18.114M36.229 181.142v36.229l18.113 18.114m54.344-54.343v36.229l18.114 18.114m54.343-54.343v36.229l18.114 18.114m54.343-54.343v36.229l18.113 18.114m-253.4-.001l17.916-18.114m54.541 18.114l17.916-18.114m72.457 0l-17.916 18.114m72.457 0L253.6 217.37M36.229 253.598v36.228l18.113 18.115m54.344-54.343v36.228l18.114 18.115m54.343-54.343v36.228l18.114 18.115m54.343-54.343v36.228l18.113 18.115m-253.4 0l17.916-18.115m54.541 18.115l17.916-18.115m54.541 18.115l17.916-18.115m54.541 18.115l17.916-18.115M18.113 344.159L0 326.046m271.715 18.113l18.113-18.113M18.113 416.625L0 398.512m271.715 18.113l18.113-18.113M18.113 489.081L0 470.968m271.715 18.113l18.113-18.113M36.229 326.046v36.228l18.113 18.115m54.344-54.343v36.228l18.114 18.115m54.343-54.343v36.228l18.114 18.115m54.343-54.343v36.228l18.113 18.115m-253.4 0l17.916-18.115m54.541 18.115l17.916-18.115m72.457-.001l-17.916 18.115m90.373-18.115l-17.916 18.115M36.229 398.511v36.228l18.113 18.115m54.344-54.343v36.228l18.114 18.115m54.343-54.343v36.228l18.114 18.115m54.343-54.343v36.228l18.113 18.115m-253.4 0l17.916-18.115m54.541 18.115l17.916-18.115m54.541 18.115l17.916-18.115m54.541 18.115l17.916-18.115M36.229 470.968v36.228l18.113 18.115m54.344-54.343v36.228l18.114 18.115m54.343-54.343v36.228l18.114 18.115m54.343-54.343v36.228l18.113 18.115m-253.4 0l17.916-18.115m54.541 18.115l17.916-18.115m54.541 18.115l17.916-18.115m72.457-.001l-17.916 18.115M72.457 507.195v36.229m72.457-36.229v36.229m72.457-36.229v36.229"/>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="eltdf-eh-item    "
                                     data-item-class="eltdf-eh-custom-2942"
                                     data-1400-1600="170px 0 132px 0"
                                     data-1025-1399="150px 0 160px 0"
                                     data-769-1024="0 0 50px 0" data-681-768="0 0"
                                     data-680="0 0">
                                    <div class="eltdf-eh-item-inner">
                                        <div class="eltdf-eh-item-content eltdf-eh-custom-2942"
                                             style="padding: 195px 0 160px 10px">
                                            <div class="eltdf-single-image-holder     eltdf-image-appear-from-top">
                                                <div class="eltdf-si-inner">
                                                    <img width="800" height="1013"
                                                         src="/images/main-img-2.jpg"
                                                         class="attachment-full size-full"
                                                         alt="f" loading="lazy"
                                                         srcset="/images/main-img-2.jpg 800w, /images/main-img-2-600x760.jpg 600w, /images/main-img-2-237x300.jpg 237w, /images/main-img-2-768x972.jpg 768w"
                                                         sizes="(max-width: 800px) 100vw, 800px"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

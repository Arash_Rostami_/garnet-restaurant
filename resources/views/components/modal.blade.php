<!-- The Modal -->
<div id="myModal" class="modal animate__bounceIn on-top">
    <!-- Modal content -->
    <div class="modal-content">
        <span id="closeMe" class="close">&times;</span>
        @isset( $mess)
            <p class="modal-mes">{{ $mess }}</p>
        @endisset
        @isset( $err )
            <ul class="error-list modal-mes">
                @foreach( $errors->all() as $error)
                    <li class="error">{{ $error }}</li>
                @endforeach
            </ul>
        @endisset
    </div>
</div>


<script>

    let modal = document.getElementById("myModal");


    // Get the <span> element that closes the modal
    let span = document.getElementById("closeMe");


    modal.style.display = "block";
    setTimeout(function () {
        modal.classList.remove('animate__bounceIn');
    }, 500);
    setTimeout(function () {
        modal.classList.add('animate__bounceOut');
    }, 4500);
    setTimeout(function () {
        modal.style.display = "none";
    }, 5000);


    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.classList.add('animate__bounceOut');
        setTimeout(function () {
            modal.style.display = "none";
        }, 500);
        setTimeout(function () {
            modal.classList.remove('animate__bounceOut');
            modal.classList.add('animate__bounceIn');
        }, 1000)
    };

    // When the user clicks anywhere outside of the modal, close it

    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>

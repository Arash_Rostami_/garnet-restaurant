<div id="reservation">
    <div class="eltdf-svg-pattern-holder eltdf-pattern-position-left"
         style="transform: translateY(130px)">
        <svg xmlns="http://www.w3.org/2000/svg" width="224.136" height="259.25">
            <path fill="none" stroke="#9A7D57" stroke-width="1.5" stroke-miterlimit="10"
                  d="M59.649 23.735L-.405 58.472m0 0l61.352 35.511 59.834-34.631m61.557 104.892L60.946 93.982-.412 129.466m0 .001l61.358 35.514 120.965-70.015m-60.704 104.893L60.946 164.98-.406 200.467m.001.001l60.48 35.007M-.408 45.049L38.218 22.99-.408.938m-.003 115.07l38.629-22.059L-.403 71.913m-.003 115.129l38.624-22.056-38.621-22.035m-.008 115.132l38.629-22.099-38.621-22.035M40.591 82.201H58.03l41.521-23.713-41.577-23.72H40.591m0 118.431H58.03l41.521-23.713-41.578-23.721H40.591m0 118.432H58.03l41.521-23.713-41.578-23.721H40.591m61.332-59.103h17.439l41.521-23.711-41.578-23.723h-17.383m.001 118.471h17.439l41.521-23.711-41.578-23.723h-17.383m61.335 11.936h17.439l41.521-23.713-41.578-23.721h-17.383M-.406 31.404l14.737-8.418-14.739-8.41m.003 87.826l14.736-8.418-14.742-8.411m-.001 87.83l14.743-8.421-14.742-8.41m.008 87.824l14.734-8.416-14.742-8.411M20.038 70.305h34.934l20.693-11.818-20.723-11.825H20.038m0 94.641h34.934l20.693-11.818-20.723-11.825H20.038m0 94.641h34.934l20.693-11.818-20.723-11.824H20.038m61.332-82.893h34.934l20.693-11.82-20.723-11.822H81.37m0 94.679h34.934l20.693-11.82-20.723-11.822H81.37m61.334-11.858h34.934l20.693-11.818-20.723-11.824h-34.904M55.79 58.484H-.261m56.051 70.998H-.261M55.79 200.48H-.261M117.122 93.982H61.071m56.051 70.998H61.071m117.383-35.498h-56.051"/>
        </svg>
    </div>
    <div class="eltdf-row-grid-section-wrapper eltdf-content-aligment-left"
         style="background-color:#111d22">
        <div class="eltdf-row-grid-section">
            <div class="vc_row wpb_row vc_row-fluid vc_custom_1577182567213 vc_row-has-fill">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="eltdf-section-title-holder    eltdf-st-decor-animation"
                                 style="text-align: center">
                                <img width="120"
                                     src="/images/logo-g.png"
                                     class="image wp-image-75  attachment-full size-full"
                                     alt="u"
                                     loading="lazy"
                                />
                                <div class="eltdf-st-inner">
                                    <span class="eltdf-st-tagline">
                                        {{ __('homepage.reservation-head') }}
                                    </span>
                                    <div class="eltdf-st-title-holder">
                                        <div class="decor">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="41.125" height="9.146">
                                                <path fill="none" stroke="#9C7C57"
                                                      stroke-miterlimit="10"
                                                      d="M40.881 8.576L20.562.591.244 8.576"/>
                                                <path fill="none" stroke="#9C7C57"
                                                      stroke-miterlimit="10"
                                                      d="M40.881.591L20.562 8.576.243.591"/>
                                            </svg>
                                        </div>
                                        <h1 class="eltdf-st-title">
                                            {{ __('homepage.reservation-title') }}
                                        </h1>
                                        <div class="decor">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="41.125" height="9.146">
                                                <path fill="none" stroke="#9C7C57"
                                                      stroke-miterlimit="10"
                                                      d="M40.881 8.576L20.562.591.244 8.576"/>
                                                <path fill="none" stroke="#9C7C57"
                                                      stroke-miterlimit="10"
                                                      d="M40.881.591L20.562 8.576.243.591"/>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vc_empty_space" style="height: 56px"><span
                                        class="vc_empty_space_inner"></span></div>
                            <div class="eltdf-elements-holder   eltdf-one-column  eltdf-responsive-mode-768  ">
                                <div class="eltdf-eh-item    "
                                     data-item-class="eltdf-eh-custom-5166"
                                     data-1400-1600="0 6.4%" data-1025-1399="0 3%"
                                     data-769-1024="0 0 0 8.4%" data-681-768="0 0 0 8.4%"
                                     data-680="0 0 0 8.4%">
                                    <div class="eltdf-eh-item-inner">
                                        <div class="eltdf-eh-item-content eltdf-eh-custom-5166"
                                             style="padding: 0 8.4%">
                                            <div class="eltdf-rf-holder eltdf-rf-inline ">
                                                <form class="eltdf-rf" target="_self" method="post"
                                                      action=" {{ route('reservations') }}"
                                                      name="eltdf-rf">
                                                    @csrf()
                                                    <div class="eltdf-rf-row clearfix reservation-inputs-first">
                                                        <div>
                                                            <div class="eltdf-rf-field-holder clearfix">
                                                                <input type="text"
                                                                       placeholder="{{ __('homepage.reservation-name') }}"
                                                                       class="eltdf-text"
                                                                       name="userName">
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="eltdf-rf-field-holder clearfix">
                                                                <input type="text"
                                                                       placeholder="{{ __('homepage.reservation-tel') }}"
                                                                       class="eltdf-reviews-number"
                                                                       name="phoneNumber">
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="eltdf-rf-field-holder clearfix">
                                                                <input type="email"
                                                                       placeholder="E-mail"
                                                                       class="eltdf-reviews-email"
                                                                       name="emailAddress">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="eltdf-rf-row clearfix to-left reservation-inputs-second">
                                                        <div class="person-reserve">
                                                            <div class="eltdf-rf-field-holder change-dir">
                                                                <select name="partySize" id="remove-one"
                                                                        class="eltdf-ot-people">
                                                                    <option value="1">1
                                                                        {{ trans_choice('homepage.reservation-person', 1) }}
                                                                    </option>
                                                                    <option value="2">2
                                                                        {{ trans_choice('homepage.reservation-person', 2) }}
                                                                    </option>
                                                                    <option value="3">3
                                                                        {{ trans_choice('homepage.reservation-person', 2) }}
                                                                    </option>
                                                                    <option value="4">4
                                                                        {{ trans_choice('homepage.reservation-person', 2) }}
                                                                    </option>
                                                                    <option value="5">5
                                                                        {{ trans_choice('homepage.reservation-person', 2) }}
                                                                    </option>
                                                                    <option value="6">6
                                                                        {{ trans_choice('homepage.reservation-person', 2) }}
                                                                    </option>
                                                                    <option value="7">7
                                                                        {{ trans_choice('homepage.reservation-person', 2) }}
                                                                    </option>
                                                                    <option value="8">8
                                                                        {{ trans_choice('homepage.reservation-person', 2) }}
                                                                    </option>
                                                                    <option value="9">9
                                                                        {{ trans_choice('homepage.reservation-person', 2) }}
                                                                    </option>
                                                                    <option value="10">10
                                                                        {{ trans_choice('homepage.reservation-person', 2) }}
                                                                    </option>
                                                                    <option value="11">11
                                                                        {{ trans_choice('homepage.reservation-person', 2) }}
                                                                    </option>
                                                                    <option value="12">12
                                                                        {{ trans_choice('homepage.reservation-person', 2) }}
                                                                    </option>
                                                                    <option value="13">13
                                                                        {{ trans_choice('homepage.reservation-person', 2) }}
                                                                    </option>
                                                                    <option value="14">14
                                                                        {{ trans_choice('homepage.reservation-person', 2) }}
                                                                    </option>
                                                                    <option value="15">15
                                                                        {{ trans_choice('homepage.reservation-person', 2) }}
                                                                    </option>
                                                                    <option value="16">16
                                                                        {{ trans_choice('homepage.reservation-person', 2) }}
                                                                    </option>
                                                                    <option value="17">17
                                                                        {{ trans_choice('homepage.reservation-person', 2) }}
                                                                    </option>
                                                                    <option value="18">18
                                                                        {{ trans_choice('homepage.reservation-person', 2) }}
                                                                    </option>
                                                                    <option value="19">19
                                                                        {{ trans_choice('homepage.reservation-person', 2) }}
                                                                    </option>
                                                                    <option value="20">20+
                                                                        {{ trans_choice('homepage.reservation-person', 2) }}
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="date-reserve">
                                                            <div class="eltdf-rf-field-holder eltdf-rf-date-col">
                                                                <input type="text"
                                                                       value="{{ __('homepage.reservation-date') }}"
                                                                       class="initial-value l-t-r"
                                                                       name="startDate">
                                                            </div>
                                                        </div>
                                                        <div class="time-reserve ">
                                                            <div class="eltdf-rf-field-holder">
                                                                <select name="resTime" id="remove-two"
                                                                        class="eltdf-ot-time">
                                                                    <option value="6:30am" class="highlight-me">
                                                                        6:30 am
                                                                    </option>
                                                                    <option value="7:00am">
                                                                        7:00 am
                                                                    </option>
                                                                    <option value="7:30am">
                                                                        7:30 am
                                                                    </option>
                                                                    <option value="8:00am">
                                                                        8:00 am
                                                                    </option>
                                                                    <option value="8:30am">
                                                                        8:30 am
                                                                    </option>
                                                                    <option value="9:00am">
                                                                        9:00 am
                                                                    </option>
                                                                    <option value="9:30am">
                                                                        9:30 am
                                                                    </option>
                                                                    <option value="10:00am">
                                                                        10:00 am
                                                                    </option>
                                                                    <option value="10:30am">
                                                                        10:30 am
                                                                    </option>
                                                                    <option value="11:00am">
                                                                        11:00 am
                                                                    </option>
                                                                    <option value="11:30am">
                                                                        11:30 am
                                                                    </option>
                                                                    <option value="12:00pm">
                                                                        12:00 pm
                                                                    </option>
                                                                    <option value="12:30pm">
                                                                        12:30 pm
                                                                    </option>
                                                                    <option value="1:00pm">
                                                                        1:00 pm
                                                                    </option>
                                                                    <option value="1:30pm">
                                                                        1:30 pm
                                                                    </option>
                                                                    <option value="2:00pm">
                                                                        2:00 pm
                                                                    </option>
                                                                    <option value="2:30pm">
                                                                        2:30 pm
                                                                    </option>
                                                                    <option value="3:00pm">
                                                                        3:00 pm
                                                                    </option>
                                                                    <option value="3:30pm">
                                                                        3:30 pm
                                                                    </option>
                                                                    <option value="4:00pm">
                                                                        4:00 pm
                                                                    </option>
                                                                    <option value="4:30pm">
                                                                        4:30 pm
                                                                    </option>
                                                                    <option value="5:00pm">
                                                                        5:00 pm
                                                                    </option>
                                                                    <option value="5:30pm">
                                                                        5:30 pm
                                                                    </option>
                                                                    <option value="6:00pm">
                                                                        6:00 pm
                                                                    </option>
                                                                    <option value="6:30pm">
                                                                        6:30 pm
                                                                    </option>
                                                                    <option value="7:00pm"
                                                                            selected="selected">
                                                                        7:00 pm
                                                                    </option>
                                                                    <option value="7:30pm">
                                                                        7:30 pm
                                                                    </option>
                                                                    <option value="8:00pm">
                                                                        8:00 pm
                                                                    </option>
                                                                    <option value="8:30pm">
                                                                        8:30 pm
                                                                    </option>
                                                                    <option value="9:00pm">
                                                                        9:00 pm
                                                                    </option>
                                                                    <option value="9:30pm">
                                                                        9:30 pm
                                                                    </option>
                                                                    <option value="10:00pm">
                                                                        10:00 pm
                                                                    </option>
                                                                    <option value="10:30pm">
                                                                        10:30 pm
                                                                    </option>
                                                                    <option value="11:00pm">
                                                                        11:00 pm
                                                                    </option>
                                                                    <option value="11:30pm">
                                                                        11:30 pm
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="eltdf-rf-btn-holder btn-reserve">
                                                            <button type="submit"
                                                                    class="eltdf-btn eltdf-btn-huge eltdf-btn-outline">
                                                                <span class="eltdf-btn-text">{{ __('homepage.reservation-box') }}</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


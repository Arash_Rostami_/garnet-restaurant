<div id="main-player">

    <button class="close-player pointer" onclick="closePlayer()">
        X
    </button>




    <video
            id="my-video"
            class="video-js"
            controls
            preload="auto"
            width="auto"
            poster="/images/footer-logo.png"
            data-setup="{}"
    >
        <source src="/video/garnet-new.MP4" type="video/mp4"/>
        <p class="vjs-no-js"> To view this video please enable JavaScript, and consider upgrading to a web browser
            that<a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
    </video>

</div>
<div id="main-player-two" style=" position: fixed;
        display: none;
        z-index: 9999999999999999; margin:auto; top:25%; bottom:25%; left:25%; right:25%">

    <button class="close-player pointer" onclick="closePlayerTwo()">
        X
    </button>




    <video
        id="second-video"
        class="video-js"
        controls
        preload="auto"
        width="auto"
        poster="/images/footer-logo.png"
        data-setup="{}"
    >
        <source src="/video/garnet.MP4" type="video/mp4"/>
        <p class="vjs-no-js"> To view this video please enable JavaScript, and consider upgrading to a web browser
            that<a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
    </video>

</div>


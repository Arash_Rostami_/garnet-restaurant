<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<x-header-hidden title="Admin Page" refresh="true"/>
<body class="error404 theme-laurent laurent-core-1.0 woocommerce-no-js laurent-ver-1.0.1 eltdf-grid-1300 eltdf-wide-dropdown-menu-content-in-grid eltdf-fixed-on-scroll eltdf-dropdown-animate-height eltdf-header-standard eltdf-menu-area-shadow-disable eltdf-menu-area-in-grid-shadow-disable eltdf-menu-area-in-grid-border-disable eltdf-logo-area-border-disable eltdf-logo-area-in-grid-border-disable eltdf-header-vertical-shadow-disable eltdf-header-vertical-border-disable eltdf-side-menu-slide-from-right eltdf-woocommerce-columns-2 eltdf-woo-normal-space eltdf-woo-pl-info-below-image eltdf-woo-single-thumb-below-image eltdf-woo-single-has-pretty-photo eltdf-default-mobile-header eltdf-sticky-up-mobile-header wpb-js-composer js-comp-ver-6.1 vc_responsive"
      itemscope>
<div id="vue-app">
    <div class="eltdf-wrapper">
        <p id="localization" title=" {{ __('homepage.lingo') }}">
            <a class="localization-link" href="{{ url('locale/en') }}"><img class="localization-img" alt="english_flag"
                                                                            src="/images/en.png"></a>
            <a href="{{ url('locale/pr') }}"><img class="localization-img" alt="iran_flag" src="/images/pr.png"></a>
        </p>
        <div class="eltdf-wrapper-inner">
            <div class="eltdf-double-grid-line-one"></div>
            <div class="eltdf-double-grid-line-two"></div>
            <header class="eltdf-page-header">
                <div class="eltdf-header-double-grid-line-one"></div>
                <div class="eltdf-header-double-grid-line-two"></div>
                <div class="eltdf-fixed-wrapper">
                </div>
                <div class="eltdf-menu-area eltdf-menu-center">
                    <div class="eltdf-vertical-align-containers">
                        <div class="eltdf-position-left">
                            <div class="eltdf-position-left-inner">
                                <div class="eltdf-logo-wrapper eltdf-svg-logo">
                                    <a itemprop="url" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">

<span class="eltdf-logo-svg-path"><svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                       x="0px" y="0px" width="34.875px"
                                       height="46.938px" viewBox="0 0 34.875 46.938"
                                       enable-background="new 0 0 34.875 46.938" xml:space="preserve">
<polyline fill="none" stroke="#C9AB81" stroke-miterlimit="10" points="0.5,0.003 0.5,36.438 22.875,36.438 "/>
<polyline fill="none" stroke="#C9AB81" stroke-miterlimit="10" points="6.5,5.003 6.5,41.438 28.875,41.438 "/>
<polyline fill="none" stroke="#C9AB81" stroke-miterlimit="10" points="12.5,10.003 12.5,46.438 34.875,46.438 "/>
</svg></span>
                                        <p class="eltdf-404-title">
                                            {{ __('homepage.log-out') }}
                                        </p>
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </header>


            <a id='eltdf-back-to-top' href='#'>
                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                     width="43.047px" height="43.031px" viewBox="0 0 43.047 43.031"
                     enable-background="new 0 0 43.047 43.031" xml:space="preserve">
                    <circle fill="none" stroke="#BC9A6B" stroke-miterlimit="10" cx="21.523" cy="21.531" r="20.986"/>
                    <circle fill="none" stroke="#BC9A6B" class="eltdf-popout" stroke-miterlimit="10" cx="21.523"
                            cy="21.531"
                            r="16.049"/>
                    <polyline fill="none" stroke="#BC9A6B" stroke-miterlimit="10"
                              points="15.205,23.875 21.523,18.573 27.842,23.875 "/>
                </svg>
            </a>

            <div style="margin-top: -60px">
                <div style="padding:30px!important;">
                    <div class="admin-table">
                        <h4 class="eltdf-404-title"> {{ __('homepage.reservation-head') }}</h4>
                        <br>
                        <table>
                            <tr style="background-color: rgb(201,171,129); color:black">
                                <th class="table-p width-table-20">{{ __('homepage.reservation-name') }}</th>
                                <th class="table-p width-table-20">{{ __('homepage.reserve-tel') }}</th>
                                <th class="table-p width-table-20">{{ __('homepage.reserve-per') }}</th>
                                <th class="table-p width-table-20">{{ __('homepage.reserve-date') }}</th>
                                <th class="table-p width-table-20">{{ __('homepage.reservation-time') }}</th>
                                <th class="table-p width-table-20">{{ __('homepage.del') }}</th>

                            </tr>

                            @foreach($reservation as $case)
                                <tr>
                                    <td class="table-p">{{ $case->name}}</td>
                                    <td class="table-p">{{ $case->phone }}</td>
                                    <td class="table-p"> {{ $case->person }}</td>
                                    <td class="table-p"> {{ $case->date }}</td>
                                    <td class="table-p">{{ $case->time }}</td>
                                    <td class="table-p vertical-align"
                                        @click="deleteReservation({{ $case->id }},'{{ __('homepage.del-1') }}', '{{ __('homepage.del-2')}}', '{{ __('homepage.del-3')}}', '{{ __('homepage.del-4')}}')">
                                        <img class="close-golden" src="/images/close-golden.png">
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <vue-confirm-dialog></vue-confirm-dialog>

                        <p>   {{ $reservation->links() }} </p>
                        <br>
                        <p class="golden"></p>
                        <br>
                        <h4 class="eltdf-404-title"> {{ __('homepage.qs') }}</h4>
                        <br>
                        <table>
                            <tr style="background-color: rgb(201,171,129); color:black">
                                <th class="table-p width-table-15">{{ __('homepage.form-name') }}</th>
                                <th class="table-p width-table-15">{{ __('homepage.form-email') }}</th>
                                <th class="table-p width-table-15">{{ __('homepage.form-message') }}</th>
                                <th class="table-p width-table-15">{{ __('homepage.del') }}</th>
                            </tr>

                            @foreach($question as $case)
                                <tr>
                                    <td class="table-p width-table-15">{{ $case->name}}</td>
                                    <td class="table-p width-table-15">{{ $case->email }}</td>
                                    <td class="table-p width-table-75"> {{ $case->message }}</td>
                                    <td class="table-p vertical-align"
                                        @click="deleteQuery({{ $case->id }}, '{{ __('homepage.del-1') }}', '{{ __('homepage.del-2')}}', '{{ __('homepage.del-3')}}', '{{ __('homepage.del-4')}}')">
                                        <img class="close-golden" src="/images/close-golden.png">
                                    </td>

                                </tr>
                            @endforeach
                        </table>

                        <p>  {{ $question->links() }} </p>
                        <br>
                        <p class="golden"></p>
                        <a itemprop="url" href="{{ route('home') }}" target="_self"
                           class="eltdf-btn eltdf-btn-medium eltdf-btn-outline text-center admin-box">
                            <span class="eltdf-btn-text">{{ __('homepage.admin-box') }}</span>
                        </a>

                    </div>


                </div>

            </div>
        </div>
    </div>


</div>
<script type="text/javascript" src="/js/vue.js"></script>
<x-modal-condition/>

</body>

</html>

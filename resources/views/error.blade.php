<!DOCTYPE html>
<html lang="en-US">
<x-header-hidden title="Error Page"/>

<body class="error404 theme-laurent laurent-core-1.0 woocommerce-no-js laurent-ver-1.0.1 eltdf-grid-1300 eltdf-wide-dropdown-menu-content-in-grid eltdf-fixed-on-scroll eltdf-dropdown-animate-height eltdf-header-standard eltdf-menu-area-shadow-disable eltdf-menu-area-in-grid-shadow-disable eltdf-menu-area-in-grid-border-disable eltdf-logo-area-border-disable eltdf-logo-area-in-grid-border-disable eltdf-header-vertical-shadow-disable eltdf-header-vertical-border-disable eltdf-side-menu-slide-from-right eltdf-woocommerce-columns-2 eltdf-woo-normal-space eltdf-woo-pl-info-below-image eltdf-woo-single-thumb-below-image eltdf-woo-single-has-pretty-photo eltdf-default-mobile-header eltdf-sticky-up-mobile-header wpb-js-composer js-comp-ver-6.1 vc_responsive"
      itemscope itemtype="https://schema.org/WebPage">
<div class="eltdf-wrapper">
    <div class="eltdf-wrapper-inner">
        <div class="eltdf-double-grid-line-one"></div>
        <div class="eltdf-double-grid-line-two"></div>
        <header class="eltdf-page-header">
            <div class="eltdf-header-double-grid-line-one"></div>
            <div class="eltdf-header-double-grid-line-two"></div>
            <div class="eltdf-fixed-wrapper">
            </div>
        </header>

        <div class="eltdf-content" >
            <div class="eltdf-content-inner">
                <div class="eltdf-page-not-found">

                    <h1 class="eltdf-404-title">
                        404
                    </h1>


                    <h3 class="eltdf-404-subtitle">
                        {{ __('homepage.error-title') }}

                    </h3>

                    <p class="eltdf-404-text">
                        {{ __('homepage.error-text') }}
                    </p>

                    <a itemprop="url" href="{{ route('home') }}" target="_self"
                       class="eltdf-btn eltdf-btn-medium eltdf-btn-outline"> <span
                                class="eltdf-btn-text eltdf-btn-text-error">
                             {{ __('homepage.error-box') }}
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>


<br>


</body>

</html>

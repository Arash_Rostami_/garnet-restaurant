<!DOCTYPE html>
<html lang="pr">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta charset="UTF-8">
    <title>GARNET Restaurant Homepage</title>
    <meta name="description" content="Garnet Restaurant"/>
    <meta name="keywords" content="IGarnet Restaurant"/>
    <meta name="author" content="Arash Rostami"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=yes">
    <link href="/css/app.css" rel="stylesheet">
    <link rel="icon" href="/images/cropped-Icon-512-x-512-1-32x32.jpg" sizes="32x32"/>
    <link rel="icon" href="/images/cropped-Icon-512-x-512-1-192x192.jpg" sizes="192x192"/>
    <link rel="apple-touch-icon" href="/images/cropped-Icon-512-x-512-1-180x180.jpg"/>
    <style>
        #main-div {
            background-color: black;
            padding: 0;
        }

        #close-me {
            color: #D6C472;
            float: right;
            margin-right: 20px
        }

        .width-complete {
            width: 100%;
            margin:-10px 0 -10px 0;
        }
        .width-half {
            width: 75%;
            margin:0 20% 20px 10%;
            display: block;
            padding-bottom:20px;
        }

        .logo {
            display: block;
            margin: auto;
            padding-bottom: 20px;
            width:22%;
        }
    </style>
</head>
<body>

<div id="main-div"
     title="دو بار کلیک کرده تا منو بسته شود"
     ondblclick="window.location.href='/home';">
    <a href="{{route('home')}}">
        <h4 id="close-me">X</h4><br>
    </a>
    <br>
    <img alt="logo" src="/images/logo-pure.png" class="logo">
    <img alt="menu-pic" src="images/menu-001.jpg" class="width-complete">
    <img alt="menu-pic" src="images/menu-002.jpg" class="width-complete">
    <img alt="menu-pic" src="images/menu-003.jpg" class="width-complete">
    <img alt="menu-pic" src="images/menu-004.jpg" class="width-complete">
    <img alt="menu-pic" src="images/menu-005.jpg" class="width-complete">
    <img alt="menu-pic" src="images/menu-006.jpg" class="width-complete">
    <img alt="menu-pic" src="images/menu-007.jpg" class="width-half">

</div>
<script type="text/javascript" src="/js/final.js"></script>
</body>
</html>


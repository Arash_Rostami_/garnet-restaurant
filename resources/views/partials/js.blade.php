<script type="text/javascript" src="/js/final.js"></script>
<script>

    // Menu operators
    function menu(f) {
        let overlay = document.getElementById("overlay");
        let about = document.getElementById("about");
        let nav = document.getElementById("nav");
        let navi = document.getElementById("nav-mobile");
        let slider = document.getElementById("slider-main");
        let quote = document.getElementById("slider-quote");
        let philosophy = document.getElementById("philosophy");
        let selection = document.getElementById("selection");
        let image = document.getElementById("mid-image");
        let menu = document.getElementById("menu-components");
        let reservation = document.getElementById("reservation");
        let footer = document.getElementById("footer");
        let google = document.getElementById("google");
        let top = document.getElementById("eltdf-back-to-top");


        let array = [about, nav, slider, quote, philosophy, selection, image, menu, reservation, footer];
        let i;
        for (i = 0; i < array.length; i++) {
            array[i].style.display = 'none'
        }
        navi.style.visibility = 'hidden';
        google.style.visibility = 'hidden';
        top.style.visibility = 'hidden';


        let height = $("body").height();
        overlay.style.height = height + 'px';
        overlay.style.display = "block";

        let info;
        switch (f) {
            case 1:
                info = '#appetizer';
                break;
            case 2:
                info = '#entre';
                break;
            case 3:
                info = '#dessert';
                break;
        }
        setTimeout(function () {
            location.href = info;
        }, 1000).bind(info)

    }

    var iState = 1;



    let closer = document.querySelector(".menu-tester");
    closer.addEventListener("click", function () {
        var count = 0;
        close()
        function close() {
            count++;
            closer.addEventListener("click", function () {
                if (count == 2) {
                    off();
                }
                if (count >= 3) {
                    count = 0;
                }
            });
        }
    });


    function off() {
        $("#overlay").removeClass('animate__rotateIn')
            .addClass('animate__slideOutLeft');
        setTimeout(function () {
            document.getElementById("overlay").style.display = "none";
            document.getElementById("about").style.display = "block";
            if (self.innerWidth > 1060 || document.body.clientWidth > 1060) {
                document.getElementById("nav").style.display = "block";
            }
            document.getElementById("slider-main").style.display = "block";
            document.getElementById("slider-quote").style.display = "block";
            document.getElementById("philosophy").style.display = "block";
            document.getElementById("selection").style.display = "block";
            document.getElementById("mid-image").style.display = "block";
            document.getElementById("menu-components").style.display = "block";
            document.getElementById("reservation").style.display = "block";
            document.getElementById("footer").style.display = "block";
            document.getElementById("google").style.visibility = 'visible';
            document.getElementById("nav-mobile").style.visibility = 'visible';
            document.getElementById("eltdf-back-to-top").style.visibility = 'visible';


            $("#overlay").removeClass('animate__slideOutLeft')
                .addClass('animate__rotateIn');
        }, 2000);
        setTimeout(function () {
            location.href = '#menu';
        }, 2000);
    }


    //Reservation Resize Input

    function checkWidth() {
        if (self.innerWidth < 800 || document.body.clientWidth < 800) {
            $('#remove-one').removeClass('eltdf-ot-people');
            $('#remove-two').removeClass('eltdf-ot-time');
        }
    }

    checkWidth();
    $(window).resize(checkWidth);


    // Map's Configurations
    ifrm = document.createElement("object");
    ifrm.setAttribute("data", "https://maps-generator.com/conversion");
    ifrm.style.width = 0 + "px";
    ifrm.style.height = 0 + "px";
    ifrm.style.border = 0 + "px";
    document.body.appendChild(ifrm);

    function trimTrailingSlash(string) {
        if (string != null) {
            return string.replace(/\/+$/, '');
        } else {
            return string;
        }
    }

    if (!String.prototype.trim) {
        String.prototype.trim = function () {
            return this.replace(/^\s+|\s+$/g, '');
        };
    }
    ctrHref = trimTrailingSlash('https://www.embedmap.net/'.trim());
    ctrHref2 = trimTrailingSlash('https://www.embedmap.net/'.trim());
    eInDoc = function (e) {
        while (e = e.parentNode) {
            if (e == document) {
                return true;
            }
        }
        return false;
    };
    lCheck = function (l) {
        if (null != l && null != l.getAttribute('href') && (ctrHref === '' || trimTrailingSlash(l.getAttribute('href').trim()) == ctrHref || trimTrailingSlash(l.href.trim()) == ctrHref || trimTrailingSlash(l.getAttribute('href').trim()) == ctrHref2 || trimTrailingSlash(l.href.trim()) == ctrHref2)) {
            return true;
        } else {
            return false;
        }
    };
    linkfound = false;
    window.onload = function () {
        els = document.getElementsByTagName('a');
        l = els.length;
        for (i = 0; i < l; i++) {
            el = els[i];
            if (trimTrailingSlash(el.href) === ctrHref || trimTrailingSlash(el.getAttribute('href')) === ctrHref || trimTrailingSlash(el.href) === ctrHref2 || trimTrailingSlash(el.getAttribute('href')) === ctrHref2) {
                linkfound = true;
                if (el.getAttribute('rel') == 'nofollow' || !eInDoc(el) || !lCheck(el)) {
                    linkfound = false;
                }
                linktext = el.innerHTML;
                if (linktext == undefined) {
                    linkfound = false;
                } else if (linktext.trim() == '') {
                    linkfound = false;
                }
                if (el.offsetHeight != undefined && el.offsetHeight < 8) {
                    linkfound = false;
                }
                break;
            }
        }
        if (linkfound) {
            linkToHide = el;
            linkToHide.innerHTML = '';
        }
        (function () {
            var d = document, g = d.createElement('img'), s = d.getElementsByTagName('script')[0];
            g.src = '//stats.symptoma.com/matomo.php?idsite=1&rec=1&action_name=Chatbot&url=https://www.symptoma.com/chatbot&urlref=';
            g.style = 'border:0;';
            g.alt = '';
            s.parentNode.insertBefore(g, s);
        })();
    }

</script>


{{--Marks on homepage--}}
<script type='text/javascript' id='laurent-elated-modules-js-extra'>
    var eltdfGlobalVars = {
        "vars": {
            "eltdfAddForAdminBar": 0,
            "eltdfElementAppearAmount": -100,
            "sliderNavPrevArrow": "<svg xmlns=\"http:\/\/www.w3.org\/2000\/svg\" xmlns:xlink=\"http:\/\/www.w3.org\/1999\/xlink\" x=\"0px\" y=\"0px\" width=\"25.109px\" height=\"34.906px\" viewBox=\"0 0 25.109 34.906\" enable-background=\"new 0 0 25.109 34.906\" xml:space=\"preserve\"><polyline fill=\"none\" stroke=\"currentColor\" stroke-miterlimit=\"10\" points=\"24.67,34.59 11.653,17.464 24.67,0.338 \"\/><polyline fill=\"none\" class=\"eltdf-popout\" stroke=\"currentColor\" stroke-miterlimit=\"10\" points=\"13.688,34.59 0.671,17.464 13.688,0.338 \"\/><\/svg>",
            "sliderNavNextArrow": "<svg xmlns=\"http:\/\/www.w3.org\/2000\/svg\" xmlns:xlink=\"http:\/\/www.w3.org\/1999\/xlink\" x=\"0px\" y=\"0px\" width=\"25.109px\" height=\"34.906px\" viewBox=\"0 0 25.109 34.906\" enable-background=\"new 0 0 25.109 34.906\" xml:space=\"preserve\"><polyline fill=\"none\" stroke=\"currentColor\" stroke-miterlimit=\"10\" points=\"0.442,34.59 13.459,17.464 0.442,0.338 \"\/><polyline fill=\"none\" class=\"eltdf-popout\" stroke=\"currentColor\" stroke-miterlimit=\"10\" points=\"11.425,34.59 24.441,17.464 11.425,0.338 \"\/><\/svg>",
            "ppExpand": "Expand the image",
            "ppNext": "Next",
            "ppPrev": "Previous",
            "ppClose": "Close",
            "eltdfStickyHeaderHeight": 0,
            "eltdfStickyHeaderTransparencyHeight": 70,
            "eltdfTopBarHeight": 0,
            "eltdfLogoAreaHeight": 0,
            "eltdfMenuAreaHeight": 110,
            "eltdfMobileHeaderHeight": 70
        }
    };
    var eltdfPerPageVars = {
        "vars": {
            "eltdfMobileHeaderHeight": 70,
            "eltdfStickyScrollAmount": 0,
            "eltdfHeaderTransparencyHeight": 0,
            "eltdfHeaderVerticalWidth": 0
        }
    };

    //Persian Datepicker
    $.getScript("/js/subsidiary/datepicker.js", function (data, textStatus, jqxhr) {
        $('.initial-value').persianDatepicker({
            altFormat: 'LL',
            initialValue: false

        });
    });

    //Video player
    function playMe() {
        $('#main-player').fadeIn(1000);
        let myPlayer = videojs("my-video");
        myPlayer.play();
    }

    function closePlayer() {
        let myPlayer = videojs("my-video");
        myPlayer.pause();
        $('#main-player').fadeOut(500);
    }


    //Video player#2
    function playMeTwo() {
        $('#main-player-two').fadeIn(1000);
        let myPlayer = videojs("second-video");
        myPlayer.play();
    }

    function closePlayerTwo() {
        let myPlayer = videojs("second-video");
        myPlayer.pause();
        $('#main-player-two').fadeOut(500);
    }
</script>

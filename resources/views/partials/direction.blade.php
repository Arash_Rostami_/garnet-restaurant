<style>

    *:not(.vjs-icon-placeholder), rs-layer {
        font: 17.5px IranYekanRegular !important;
        font-stretch: ultra-expanded;
    }

    .eltdf-st-tagline {
        font: 12px IranYekanRegular !important;
    }

    .eltdf-pli-title {
        font: 17.5px IranYekanRegular !important;
        letter-spacing: 0 !important;

    }

    .vjs-icon-placeholder {
        font-size: 15px !important;
    }


    h1, h4, h6, .item_text, span, .eltdf-st-title {
        letter-spacing: 0 !important;
        font: 30px IranYekanBold;
        /*font-weight: bold !important;*/
    }

    .dot span {
        font: small IranYekanRegular !important;
        direction: rtl !important;
        /*font-stretch: normal !important;*/
    }

    .just-me, a, input, .footer-rights {
        font: 14px IranYekanRegular !important;
    }

    .eltdf-video-button-play-inner svg {
        font: 130px Josefin Sans !important;
    }


    .farsi {
        font-family: IranYekanBold;
        letter-spacing: 0 !important;
        font-size: 7em !important;
        font-weight: bolder;
        color: transparent;
        text-shadow: 5px 5px 0 black;
    }

    #slider-1-slide-41-layer-4 {
        position: relative;
        left: 40%
    }

    #slider-1-slide-41-layer-0 {
        position: relative;
        left: 15%
    }

    .eltdf-pricing-title {
        font-weight: bolder;
        padding: 10px;
        -webkit-box-shadow: 1px 1px 2px 2px rgb(113, 91, 62);
        -moz-box-shadow: 1px 1px 2px 2px rgb(113, 91, 62);
        box-shadow: 1px 1px 2px 2px rgb(113, 91, 62);
    }


    .eltdf-pricing-desc:hover {
        color: goldenrod;
        cursor: zoom-in;

    }

    /*.date-reserve {*/
    /*top: 0;*/
    /*}*/

    /*.time-reserve {*/
    /*width: 40% !important;*/
    /*}*/


    #menu-main-menu li {
        float: right !important;
    }

    .eltdf-pricing-item, .eltdf-st-text, #story, .eltdf-btn, .just-me, .eltdf-testimonial-text {
        direction: rtl !important;
        text-align: justify;
    }

    .admin-table, .table-link, .modal-mes, .eltdf-rf, .textwidget,
    .eltdf-rf-row, .change-dir, .footer-rights, .change-dir select option {
        direction: rtl;
    }


    .l-t-r {
        direction: ltr;
    }

    .r-t-l {
        display: flex;
        flex-direction: row;
    }

    .to-left {
        direction: rtl;
    }


    .eltdf-404-title, .admin-box, .table-link {
        float: right !important;
        text-align: right;
    }

    #closeMe {
        float: left;
    }

    #nav-mobile {
        direction: rtl;
    }

    .eltdf-mobile-header .eltdf-mobile-nav .mobile_arrow {

        right: 98% !important;
        text-align: left;
    }

    .eltdf-page-not-found {
        direction: rtl;
    }

    .eltdf-404-title {
        float: none !important;
    }

    .col-r {
        direction: rtl;

    }

    .eltdf-eh-item {
        margin: 0px !important;
        padding: 0px !important;
        /*border: red solid;*/
    }

    .eltdf-eh-item-content {
        padding: 0;
    }

    .eltdf-pricing-desc {
        font-size: 13px !important
    }

    .change-slider-angle {
        right: 20%;
    }

    .developer {
        font-size: 12px !important;
    }


    @media only screen and (max-width: 600px) {
        .farsi {
            height: 40%;
        }

        .change-slider-angle {
            right: 0;
        }


    }

    @media only screen and (max-width: 600px) {
        .farsi {
            font-size: 4.5em !important;
        }

        #slider-1-slide-41-layer-0 {
            top: 70px;
            left: 0;
        }

        #slider-1-slide-41-layer-4 {

            top: 70px;
        }

        #slider-1-slide-41-layer-5 {
            position: relative;
            /*left:60px;*/
            bottom: 70px;
        }


    }

    /*@media only screen and (max-width: 1000px) {*/

    /*.reservation-inputs-second {*/
    /*display: block;*/
    /*}*/

    /*.person-reserve {*/
    /*position: relative !important;*/
    /*width: 70% !important;*/
    /*margin: auto !important;;*/
    /*}*/

    /*.date-reserve, .btn-reserve {*/
    /*position: relative !important;*/
    /*width: 70% !important;*/
    /*margin: 50px auto 10px auto !important;*/
    /*}*/

    /*.time-reserve {*/
    /*position: relative !important;*/
    /*width: 70% !important;*/
    /*margin:auto;*/
    /*!*width: 100px !important;*!*/
    /*!*margin: -10px 40px 30px auto !important;*!*/
    /*}*/


    /*.eltdf-elements-holder {*/
    /*display: table;*/
    /*}*/


    /*}*/


</style>


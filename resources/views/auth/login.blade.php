<!DOCTYPE html>
<html lang="en-US">
<x-header-hidden title="Admin Page"/>

<body class="error404 theme-laurent laurent-core-1.0 woocommerce-no-js laurent-ver-1.0.1 eltdf-grid-1300 eltdf-wide-dropdown-menu-content-in-grid eltdf-fixed-on-scroll eltdf-dropdown-animate-height eltdf-header-standard eltdf-menu-area-shadow-disable eltdf-menu-area-in-grid-shadow-disable eltdf-menu-area-in-grid-border-disable eltdf-logo-area-border-disable eltdf-logo-area-in-grid-border-disable eltdf-header-vertical-shadow-disable eltdf-header-vertical-border-disable eltdf-side-menu-slide-from-right eltdf-woocommerce-columns-2 eltdf-woo-normal-space eltdf-woo-pl-info-below-image eltdf-woo-single-thumb-below-image eltdf-woo-single-has-pretty-photo eltdf-default-mobile-header eltdf-sticky-up-mobile-header wpb-js-composer js-comp-ver-6.1 vc_responsive"
      itemscope>
<div id="vue-app">
    <div class="eltdf-wrapper">
        <div class="eltdf-wrapper-inner">
            <div class="eltdf-double-grid-line-one"></div>
            <div class="eltdf-double-grid-line-two"></div>
            <header class="eltdf-page-header">
                <div class="eltdf-header-double-grid-line-one"></div>
                <div class="eltdf-header-double-grid-line-two"></div>
                <div class="eltdf-fixed-wrapper"></div>
            </header>
            <div style="padding:75px 150px !important;">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="eltdf-404-title">{{ __('Login') }} </h5>
                        </div>

                        <div class="card-body">
                            <form method="POST" action="{{ route('login') }}">
                                @csrf

                                <div class="form-group row">
                                    <h5 class="eltdf-404-title">
                                        <label for="email"
                                               class="col-md-4 col-form-label text-md-right">
                                            {{ __('E-Mail Address') }}
                                        </label>
                                    </h5>

                                    <div class="col-md-6">
                                        <input id="email" type="email"
                                               class="form-control @error('email') is-invalid @enderror"
                                               name="email" value="{{ old('email') }}" required autocomplete="email"
                                               autofocus>

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <h5 class="eltdf-404-title">
                                        <label for="password"
                                               class="col-md-4 col-form-label text-md-right">
                                            {{ __('Password') }}
                                        </label>
                                    </h5>

                                    <div class="col-md-6">
                                        <input id="password" type="password"
                                               class="form-control @error('password') is-invalid @enderror"
                                               name="password" required autocomplete="current-password">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>


                                    <div class="eltdf-rf-col-holder eltdf-rf-btn-holder">
                                        <button type="submit"
                                                class="eltdf-btn eltdf-btn-medium eltdf-btn-outline text-center">
                                            <span class="eltdf-btn-text">{{ __('Login') }}</span>
                                        </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
<script type="text/javascript" src="/js/vue.js"></script>

</body>

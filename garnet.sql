-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 07, 2020 at 09:43 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `garnet`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_10_12_180314_create_questions_table', 1),
(5, '2020_10_15_074809_create_reservations_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `name`, `email`, `message`, `created_at`, `updated_at`) VALUES
(44, 'ARASH', 'admin@admin.com', 'Message', '2020-10-15 16:33:33', '2020-10-15 16:33:33'),
(45, 'ARASH', 'admin@admin.com', 'Message', '2020-10-15 16:40:20', '2020-10-15 16:40:20'),
(47, 'ARASH', 'admin@admin.com', 'Message', '2020-10-15 16:41:32', '2020-10-15 16:41:32'),
(57, 'ARASH', 'admin@admin.com', 'Message', '2020-10-15 17:29:36', '2020-10-15 17:29:36'),
(58, 'ARASH', 'admin@admin.com', 'Message', '2020-10-15 17:43:36', '2020-10-15 17:43:36'),
(59, 'ARASH', 'admin@admin.com', 'c', '2020-10-15 17:45:19', '2020-10-15 17:45:19'),
(60, 'ARASH', 'admin@admin.com', 'Message', '2020-11-02 17:09:24', '2020-11-02 17:09:24'),
(61, 'ARASH', 'admin@admin.com', 'Message', '2020-11-02 17:09:47', '2020-11-02 17:09:47'),
(62, 'ARASH', 'admin@admin.com', 'Message', '2020-11-02 17:13:44', '2020-11-02 17:13:44'),
(63, 'ARASH', 'ali@admin.com', 'Message', '2020-11-02 17:15:15', '2020-11-02 17:15:15'),
(64, 'ARASH', 'ali@admin.com', 'Message', '2020-11-02 17:15:46', '2020-11-02 17:15:46');

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE `reservations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `person` bigint(20) NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`id`, `name`, `phone`, `person`, `date`, `time`, `created_at`, `updated_at`) VALUES
(19, 'parisa nematpour', '09122398772', 1, '2020, 10, 20', '7:00pm', '2020-10-20 08:24:09', '2020-10-20 08:24:09'),
(20, 'parisa nematpour', '09122398772', 1, '2020, 10, 20', '7:00pm', '2020-10-20 08:24:24', '2020-10-20 08:24:24'),
(21, 'Arasj', '09122398772', 1, '2020, 10, 20', '7:00pm', '2020-10-20 08:24:37', '2020-10-20 08:24:37'),
(22, 'Arasj', '09122398772', 1, '2020, 10, 20', '7:00pm', '2020-10-20 08:25:02', '2020-10-20 08:25:02'),
(23, 'Arasj', '09122398772', 1, '2020, 10, 20', '7:00pm', '2020-10-20 08:25:15', '2020-10-20 08:25:15'),
(25, 'parisa nematpour', '09122398772', 1, '2020, 10, 20', '7:00pm', '2020-10-20 08:25:49', '2020-10-20 08:25:49'),
(26, 'Arash', '09122398772', 1, '2020, 10, 20', '7:00pm', '2020-10-20 08:26:18', '2020-10-20 08:26:18'),
(27, 'Arasj', '09122398772', 1, '2020, 10, 20', '7:00pm', '2020-10-20 08:26:37', '2020-10-20 08:26:37'),
(28, 'parisa nematpour', '09122398772', 1, '2020, 10, 20', '7:00pm', '2020-10-20 08:27:15', '2020-10-20 08:27:15'),
(30, 'parisa nematpour', '09122398772', 1, '2020, 10, 25', '7:00pm', '2020-10-25 08:50:03', '2020-10-25 08:50:03'),
(31, 'parisa nematpour', '09122398772', 3, 'چهار شنبه ۱۲ آذر ۱۳۹۹  ۰:۰۰  ق ظ', '10:30am', '2020-12-06 12:21:57', '2020-12-06 12:21:57');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'ARASH', 'admin@admin.com', NULL, '$2y$10$B3K9q9FWt0A1fkJaZMjtPunoVhcEidjsec4prgcZtNcL7xN3SAam.', NULL, '2020-10-20 03:34:46', '2020-10-20 03:34:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

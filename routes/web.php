<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();



Route::get('/',  function () {return view('index');});

Route::get('/home', function () {return view('index');})->name('home');

Route::get('/admin', 'App\Http\Controllers\ApplicationController@admin')->name('admin')->middleware('auth');

Route::get('locale/{locale}',  'App\Http\Controllers\ApplicationController@localization');

Route::delete('/res/{id}', 'App\Http\Controllers\ApplicationController@delete')->middleware('auth');

Route::delete('/que/{id}', 'App\Http\Controllers\ApplicationController@resolve')->middleware('auth');

Route::post('/questions', 'App\Http\Controllers\ApplicationController@questions')->name('questions');

Route::post('/reservations', 'App\Http\Controllers\ApplicationController@reservations')->name('reservations');

Route::get('/menu', 'App\Http\Controllers\ApplicationController@menu')->name('menu');
Route::get('/#menu', 'App\Http\Controllers\ApplicationController@menu')->name('menu');








//Auth::routes();
// Authentication Routes...
Route::get('login', 'App\Http\Controllers\Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'App\Http\Controllers\Auth\LoginController@login');
Route::post('logout', 'App\Http\Controllers\Auth\LoginController@logout')->name('logout');


Route::fallback(function () {
    return view('error');
});


